# 予告サイトのリリース

いよいよ、作成したアプリケーションを[Heroku](https://www.heroku.com/)上で動かし、世界中からアクセスできるようにします。

まず、Herokuでアカウント作成、およびHerokuのコマンドをインストールしておいてください。

では、Herokuにアプリケーションをリリースする場所を作成します。
プロジェクトのトップディレクトリ、MyProjectで以下のコマンドを打ちます。

```
heroku create
```

ログインを求められるので、Herokuに登録したメールアドレスとパスワードを入れてください。
コマンドが成功すると、herokuにリリース用のGitリポジトリが作成されます。こんな感じの出力がされます。

```
Creating app... done, ⬢ blooming-savannah-30816
https://blooming-savannah-30816.herokuapp.com/ | https://git.heroku.com/blooming-savannah-30816.git
```
二行目の最初が、アプリがリリースされるURL、|の後が、リリース用のGitリポジトリです。このリポジトリにブランチをpushするとリリースが行われます。ではmasterブランチをpushします。

```
git checkout master
git pull origin master
git push heroku master
```

一行目でローカルのmasterブランチに移動、2行目でGitHubのmasterブランチをローカルのmasterブランチに同期、3行目でローカルのmasterブランチをherokuにpushしています。

では、確認しましょう。

```
heroku open
```

ブラウザが開き、アプリケーションが表示されたら完成です。
このURLで世界中にアプリケーションが公開されています。
サイトを他の人に見てもらいたい時はURLを共有してあげましょう。

# 確認
上記の要領でアプリケーションが公開されていたらOKです。
