# Todoを登録するUI作成
Todoを登録するUIを作成していきます。
いつも通りfeatureブランチを作成します。

```sh
git checkout master
git pull origin master
git checkout -b feature/todoUI
```

src/main/resources/static/templates/todo.htmlを作成します。

```html
<div>
	<div>
		<input tyle="text" v-model="newTitle" />
		<button @click="add">TODOの追加</button>
	</div>
	<draggable label="TODO" v-model="todos" class="dragArea"
		:options="{group:'todo'}" @change="(event)=>{change(event, 'todo')}">
	<div v-for="todo in todos" class="todo">{{todo.title}}</div>
	</draggable>
	<draggable label="DOING" v-model="doings" class="dragArea"
		:options="{group:'todo'}" @change="(event)=>{change(event, 'doing')}">
	<div v-for="doing in doings" class="todo">{{doing.title}}</div>
	</draggable>
	<draggable label="DONE" v-model="dones" class="dragArea"
		:options="{group:'todo'}" @change="(event)=>{change(event, 'done')}">
	<div v-for="done in dones" class="todo">{{done.title}}</div>
	</draggable>
	<br style="clear: both" />
</div>
```

src/main/resources/static/js/todo.js を作成します。

```js
function showTodo(projectName) {
	axios.get("api/projects/" + projectName).then((response) => {
		var groupedTodos = {};
		response.data.todos.forEach((todo) => {
			if (!groupedTodos[todo.status]) {
				groupedTodos[todo.status] = [];
			}
			groupedTodos[todo.status].push(todo);
		})
		return { project: response.data, groupedTodos: groupedTodos };
	}).then((data) => {
		new Vue({
			el: '#contents',
			/** @inject("todoView.html") */
			template: null,
			data: {
				newTitle: "",
				todos: data.groupedTodos["todo"] || [],
				doings: data.groupedTodos["doing"] || [],
				dones: data.groupedTodos["done"] || []
			},
			methods: {
				add: function (url) {
					var todo = { title: this.newTitle, status: "todo" }
					this.save(todo, todo => {
						this.todos.push(todo);
						this.newTitle = ""
					})
				},
				change: function (event, status) {
					if (!event.added) return;
					var todo = event.added.element;
					todo.status = status;
					this.save(todo, () => { });
				},
				save: function (todo, then) {
					axios.post("api/projects/" + data.project.key + "/todos", todo)
						.then((response) => then(response.data))
						.catch((error) => console.log(error));
				}
			}
		})
	}).catch(e => console.log(e));
}
```

あとは、app.jsを変更し、http://localhost:8080/ にアクセスしたら今までのページを、http://localhost:8080/#XXX にアクセスしたら、TODOアプリを表示するようにします。

```
/** @injectJS("js/registMail.js") */
const showRegistMail = null;
/** @injectJS("js/todo.js") */
const showTodo = null;
if (location.hash.startsWith("#/todo/")) {
    showTodo(location.hash.substring("#/todo/".length));
} else {
	showRegistMail();
}
```

これでアプリケーションが作成できました。
localhost:8080#/todo/XXXX
にアクセスしてみましょう。
TODOアプリが使えればOKです。
![](images/todo_app.png)

コミットします。

```
git add --all #本当はサボらずに、必要なファイルを人づずつaddします。
git commit -m "TODOアプリ画面の完成"
git push origin feature/todoUI
```

プッシュできたらいつもどおりPull Requestを作成して、masterにマージします。

# 確認
* 上記の様にTODOアプリが利用できること。
* GitHubのmasterブランチにコミットが入っていること。
* 本番環境でアプリが使えること。
が出来ればOKです。
