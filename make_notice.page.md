# 予告ページの作成

さて、引き続き開発を続けていきます。
まずは、GitHubでmasterブランチが更新されているので、それを取得します。
masterブランチに移動し、

```bash
git checkout master
```

リモートのmasterブランチの内容を取得します。

```
git pull origin master
```

これによりmasterがGitHubと同期化されます。
再び、作業ブランチを作成します。

```
git branch feature/noticePage
```

作成した作業ブランチに移動します。

```
git checkout feature/noticePage
```

さて、作業を開始します。

## アプリケーションの作成
最初のWebページを作成します。
src/main/resources/staticフォルダを右クリック、New→Fileで、index.htmlというファイルを作成します。
できたindex.htmlに、以下の内容を記載します。

```
<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8>
<title>新しいサービス</title>
</head>
<body>新しいサービスを開始します。</body>
</html>

```
index.htmlは[HTML](https://ja.wikipedia.org/wiki/HyperText_Markup_Language)という言語で作成されています。\<タグ\>内容\</タグ\>のように不等号でくくられた構造を使って画面を表現します。HTMLとCSS、JavaScriptを組み合わせることでとてもリッチな画面を作成することができます。
これで、サイトのページが作成されました。
いよいよ、サイトを起動します。

SPRING INITIALIZER は、Java製のWebアプリケーションサーバを起動するソースコードも作成してくれています。

src/main/java/com.ukiuni.education.myproject にMyprojectApplication.javaというファイルがあるので、このファイルを右クリックし、Run as→Java Applicationを押します。
引き続き、ブラウザで http://localhost:8080 にアクセスします。
画面に「新しいサービスを開始します。」と表示されれば成功です。
これでWebアプリケーションサーバが起動しました。

コミットします。

```
git add --all #本当はサボらずに、必要なファイルを人づずつaddします。
git commit -m "予告サイトの作成"
git push origin feature/noticePage
```

プッシュができたらGitHub上でPullRequestをマージしてください。


# 確認
上記通り、ブラウザに「新しいサービスを開始します。」と表示されていればOKです。また、GitHub上でCommitが入っていることを確認してください。
確認できたら、EclipseのConsoleに表示されている赤い四角ボタンを押して、アプリケーションを終了させておいてください。
