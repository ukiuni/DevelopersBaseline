# もし困ったら
チャット、もしくは課題票でヘルプを得ることができます。  
チャットは[Gitter](https://gitter.im/DevelopersBaseLine)です。  
課題票は[GitHubのissue](https://github.com/DevelopersBaseLine/document/issues)で受け付けています。  
遠慮なく投稿してください。


また、「この文章はこんな風に書いてあったらもっとわかりやすいのに」と発見したら、[こちら](https://github.com/DevelopersBaseLine/document/pulls)にプルリクエストを挙げていただけると、筆者が喜ぶと共に、本書を読む他の誰かの偉大な助けと鳴るでしょう。プルリクエストの方法をご存じない方は、本書で学ぶことができます。