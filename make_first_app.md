# アプリケーションの予告サイトの公開
Spring Boot（ http://projects.spring.io/spring-boot/ ）という仕組みを使ってWebサイトを構築します。
画面はHTMLという技術で作成します。
作成したらGitリポジトリにコミットします。Gitを使うと、作ったもののバージョンを管理することができます。
ソースコードの共有のために、今回はGitHubというサービスを使います。
Gitでは、自分のPCの中だけでバージョン管理を行うことができますが、チームで作るものを共有するためにはファイルを共有するための場所が必要になります。今回はGitHubを利用します。

## 空のリポジトリをプッシュする。
さて、早速、作っていきます。
最初に行うのは、ローカルに作業ディレクトリを作成しGitHubにpushすることです。
ターミナルを開いて、ディレクトリを作成します。

```
mdir MyProject
cd MyProject
```

ディレクトリを作成したら、そのディレクトリをGitプロジェクト化します。

```
git init
```

gitプロジェクト化を行ったら、まず最初のコミットを行います。最初のコミットは空で行います。

```
git commit --allow-empty -m "first commit"
```

これで、空のコミットを行うことができました。

ローカルのブランチをGitHubにアップロードします。
最初にGitHubでリポジトリを作成します。ブラウザで https://github.com にアクセスしてください。まだGitHubアカウントをお持ちでない場合はSign Upからアカウントを作成してください。持っている方はSign Inからサインインしてください。その後、ヘッダの＋ボタンからNew repositoryを選択してください。
![](/images/new_repository.png)
続いて表示される画面で作りたいリポジトリ名を入力してください。名前は任意ですが、以下ではMyProjectと入力したものとして説明を進めていきます。
![](/images/create_repository.png)

もし、Organizationにリポジトリを作りたい場合はそのOrganizationを選択してください。
OrganizationとはGitHubのグループのようなものです。ユーザは複数のOrganizationに属することができ、Organizationは複数のリポジトリを持つことができます。
Privateだと限られた人のみが見ることができますが、有料です。
他の人に公開されても問題なければPublicを選択します。
入力したら、Create repositoryボタンを押します。

次に、このリポジトリにローカルブランチをアップロードします。
まず、GitHubのリポジトリをこのGitプロジェクトに設定します。

```bash
git remote add origin https://github.com/XXXXXX/MyProject.git
```

XXXXXはユーザ名です。
httpから始まるURLはGitHubで作成したリポジトリのURLです。GitHubのリポジトリのの画面で取得できます。
![](/images/repository_url.png)
このURLを、"origin"というリポジトリの名前で設定しています。
続いてGitHubにアップロードを行います。

```bash
git push origin master
```

pushというのが、別のリポジトリにGitプロジェクトを送るコマンドです。
Gitでは「ブランチ」というものを作成してソフトウェアを成長させていきます。ブランチとは日本語では枝のことです。枝を作成し、それを幹に戻していくことで開発を進めていきます。
masterというのはGitプロジェクトを作成した時に最初に作られるブランチです。git push origin masterとは、「masterブランチをorigin（上で設定したGitHubのリポジトリ）にpushします。」という意味です。

これでGitHubに空のブランチをpushすることができました。
本研修では、Gitへのコミットは”A successful Git branching model”( http://nvie.com/posts/a-successful-git-branching-model/ )を簡略化した方式で行います。
すわなち、masterブランチを最新ブランチとし、開発中はfeatureブランチを使用します。
featureブランチからmasterブランチへのPull Requestを作成し、レビューを行い、よければmasterブランチにコミットする方式を取ります。

# プロジェクトの初期化

さて、引き続き、ディレクトリをGradleプロジェクトにします。
Gradleプロジェクトを作成する前に、あらたなる機能開発なので、Gitのfeatureブランチを作成します。機能開発ブランチです。

```bash
git branch feature/initProject
git checkout feature/initProject
```

これで、masterブランチを元にfeature/initProjectブランチが作成されました。このブランチを更新、コミットを行い、最終的にmasterと比較してレビューを行い、masterに取り込むことで開発を進めていきます。

さて、開発を進めていきます。ディレクトリをGradleプロジェクトにします。

```bash
gradle init --type java-library
```

Gradleとはビルドツールと呼ばれているものの一つです。
ソフトウェアは、ソースコードから適切に動くように組み立てる必要があります。この作業をビルドと言います。ビルドツールとはビルドを手伝ってくれるツールです。
Javaのビルドツールの有名なものにはAnt、Maven、Gradleがあります。
Antはビルドの手順をXMLに書いておく必要があります。
Mavenはビルド手順やディレクトリ構成に決まりを持たせることで、ある程度手順を記載しなくてもビルドができます。ただし、ライブラリを使用したい場合はやはりXMLを記載する必要があります。
そして、現在JavaのビルドツールといえばGradleです。XMLを記載しなくても良くなりました。その代わりにGroovyというプログラミング言語でビルド手順を記載します。
他のツールにおいてもそうですが、現在においてXMLを記載しなくては使えないツールは往々にして時代遅れと思えます。XMLを記載しなければならないか、そうでないかを、技術を評価する際の指針にすると良いでしょう。
さて、コマンドの説明に戻ります。


```
gradle init --type java-library
```

gradle はgradleを起動するコマンドです。その後にinitをつけて実行することで現在ターミナルがいるディレクトリをgradleプロジェクトにしてくれます。gradleプロジェクトとは、そのディレクトリにbuild.gradle等、いろいろなファイルが存在する状態になっていることです。コマンドの実行後にそのディレクトリの中身を見てみましょう。幾つかのファイルが出来上がっています。
initの後の -type java-libraryは、これをつけておくとbuild.gradleにちょっとした手順を入れておいてくれるのでつけています。
ディレクトリをgradleプロジェクトにしたら、同様にEclipseプロジェクトにもしていきます。このディレクトリをEclipseプロジェクトにする作業はgradleがしてくれます。build.gradleを編集します。build.gradleを開くと、apply plugin: 'java’という行があるので、その下に
apply plugin: 'eclipse'
という行を追加します。この行を追加することで、GradleにEclipse関連の様々な機能が追加されます。
行を追記して保存したら、下のコマンドを実行します。

```bash
gradle eclipse
```

これで、ディレクトリがEclipseプロジェクトになりました。ディレクトリの中に、.classpathや.projectなどのファイルが出来上がっています。
ディレクトリをEclipseで開いてみましょう。Eclipseを起動し、ツールバーのFile→Import→Existing Project into Workspace、Select root directoryで先ほど作成したディレクトリを指定し、Finishボタンを押します。これで先ほど作成したプロジェクトがEclipseで編集できるようになりました。
さて、プロジェクトの設定完了までもう少しです。
実は先ほど作成した.classpathファイルですが、あなたのパソコンでしか動かない設定が入っています。Javaでは、.jarファイルというものを使って、様々な作成済みライブラリを利用してアプリケーションを作成していくのですが、この.jarファイルがあなたのパソコン独自の場所を指し示しているのです。これをGitHubにアップロードしてしまうと、他の人がダウンロードした時に動かなくなってしまいます。よって、Eclipseのファイル群をコミットしないようにします。
Gitでは.gitignoreというファイルにコミットしないファイルを記載することができます。
ターミナルで以下のコマンドを打ちます。

```bash
echo .classpath >> .gitignore
echo .project >> .gitignore
echo .settings >> .gitignore
```

ついでに、eclipseが生成するファイルもGitにコミットしないようにします。

```bash
echo bin >> .gitignore
```

echoというコマンドはその後に続く文字列を出力するコマンドです。>>をつけることでその後に続くファイルに出力を追記することができます。(Windowsにはこの便利機能がないと思いますので、.gitignoreをテキストエディタで作成するか、Linuxをインストールし直すか、Macを購入して下さい。)
さて、Gradleプロジェクトの設定が終わったので、作業内容をローカルのGitリポジトリにコミットします。

```bash
git add --all
git commit -a -m "Gradleプロジェクトの設定"
```

引き続き、feature/initProject をGitHubにプッシュします。

```bash
git push origin feature/initProject
```

これでGitHubに作業がプッシュされました。
この変更をGitHub上でmasterに入れます。
ブラウザでhttps://github.comにアクセスし、作成したリポジトリを開いてください。
上方のメニューのPull Requestを開いて、New pull requestボタンを押し、masterブランチとfeature/initProjectを比較、Create pull requestを押してください。
どんな変更をしたか入力する欄が表示されるため、「Gradleプロジェクトの設定」と入力し、Create pull requestボタンを押してください。するとPull Requestが作成されます。
Pull Requestとはその名の通り、とり込み要求です。Pull Requestの内容が正しいかを誰か他の人に見てもらってください。この見てもらう作業の事をレビューと言います。
レビューをする人は、正しいことを確認してください。この変更が何故正しいといえるかを考えて、もし質問等があったら、このPull Requestにコメントを入れてください。ソースコードの行をクリックすると、そこにコメントを入れることができますし、Pull Request全体に対するコメントでしたら、Pull RequestのConversationタブから入力することができます。レビューする人は責任を持って正しいといえるまでしっかり見てください。大丈夫そうなら「大丈夫そう」とコメントを入れてMerge pull requestボタンを押してください。これにより、feature/initProjectブランチがmasterブランチに取り込まれます。

これが一連のコミットの流れです。
さて、引き続き開発を続けていきます。
続いては、アプリケーションの予告ページを作成し、サーバ上で動作するようにします。
まずは、GitHubでmasterブランチが更新されているので、それを取得します。
masterブランチに移動し、
```bash
git checkout master
```

リモートのmasterブランチの内容を取得します。
git pull origin master
これによりmasterがGitHubと同期化されます。
再び、作業ブランチを作成します。
git branch feature/noticePage
作業ブランチに移動します。
git checkout feature/noticePage
さて、作業を開始します。

## アプリケーションの作成

今回はWebアプリケーションを開発するのにSpring Bootというフレームワークを利用します。
GradleにSpring Bootを使用することを記載します。
build.gradleに、

```gradle:build.gradle
apply plugin: 'spring-boot'
buildscript {
  repositories {
    mavenCentral()
  }
  dependencies {
    classpath("org.springframework.boot:spring-boot-gradle-plugin:1.4.0.RELEASE")
  }
}
```

を追加します。
また、dependenciesの中に

```
compile 'org.springframework.boot:spring-boot-starter-web'
```

を追加します。
ファイルを配置するために、一つディレクトリを作成します。

```bash
mkdir -p src/main/resources/
```


引き続き、ターミナルのプロジェクトのトップディレクトリで

```bash
gradle clean;gradle eclipse
```

を実行します。Eclipseの設定が再構築されるので、Eclipse上でプロジェクトを選択し、右クリック、Refreshを選択します。
これでSpring bootのライブラリがロードされました。
早速Webサーバを作成します。

```
package org.ukiuni.education;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
public class Main {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(Main.class, args);
	}
}
```
import から始まる行はその他の行を書いた後にCommand+Shift+oを押すとEclipseが勝手に書いてくれるので、それ以外を書きます。
これでJava製のWebアプリケーションサーバが完成しました。
次に最初のWebページを作成します。
eclipseでsrc/mainディレクトリを右クリックしてresourceというディレクトリを作成します。続いてEclipseの src/main/resources を右クリックし、New→Other、General→Folderで、staticという名前のディレクトリを作成します。
次はstaticフォルダを右クリック、New→Other、Web→HTML Fileで、index.htmlというファイルを作成します。
できたindex.htmlの&lt;body&gt;と&lt;/body&gt;の間に、「新しいサービスを開始します。」と入力します。
これで、サイトのページが作成されました。
いよいよ、サイトを起動します。
Mainクラスを右クリックし、Run as→Java Applicationを押します。
引き続き、ブラウザで http://localhost:8080 にアクセスします。
画面に「新しいサービスを開始します。」と表示されれば成功です。
これでWebアプリケーションサーバが起動しました。おめでとうございます。
サイトができたので、テストを作成します。

## テスト

テストでは、Chromeブラウザを自動操作して、http://localhost:8080 にアクセスすると「新しいサービスを開始します。」と表示されることを確認します。
私事ですが、私はソフトウェアがブラウザを自動操作する様子がとても好きです。自分が楽をしている間にコンピュータが一生懸命働いてくれる感がとても好きです。
テストにはSeleniumというソフトウェアを利用します。
GradleにSeleniumを使うことを設定します。
build.gradleのdependenciesの中に

```groovy build.gradle(抜粋)
compile 'org.seleniumhq.selenium:selenium-java:2.41.0'
```

を追加します。
Eclipseでプロジェクトを右クリック、Gradle(STS)→Refresh Allを行います。
次にchromedriverというchromeを動作させるためのソフトウェアをダウンロードしてきます。
https://sites.google.com/a/chromium.org/chromedriver/downloads
ダウンロードしたものを解凍すると、chromedriver(Windowsであればchromedriver.exe)というファイルができるので、プロジェクトのトップにtestというディレクトリを作って置きます。Eclipseのプロジェクトを右クリック、New→Folderでtestというディレクトリを作成し、chromedriverをドラッグ&ドロップでtestフォルダに入れてください。
macの場合は、実行権限を付与しておいてください。ターミナルにて、chromedriverを配置したディレクトリで、
```
chmod +x chromedriver
```
と入力します。
これで、Seleniumを動かす準備ができたので、次はテストコードを書いていきます。
Eclipseでsrc/test/javaというディレクトリを作成し、src/test/javaで右クリック、New→Classで、TestMainというクラスを作成してください。パッケージはtest.org.ukiuni.educationとしてください。
テストコードは以下です。先ほどもそうでしたが、importから始まる場所は他の部分のコードを書いておけばEclipseでCommand+Shift+oで自動で入力してくれるので、他の部分のコードを先に書くと良いです。

```java
package test.ukiuni.education;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.ukiuni.education.Main;
public class TestMain {
	private WebDriver driver;
	private ConfigurableApplicationContext context;
	
	@Before
	public void init() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./test/chromedriver");
		System.setProperty("server.port", "9999");
		driver = new ChromeDriver(new ChromeOptions());
		context = SpringApplication.run(Main.class, new String[] {});
	}

	@After
	public void tearDown() {
		driver.quit();
		SpringApplication.exit(context);
	}

	@Test
	public void testIndex() {
		driver.get("http://localhost:9999");
		Assert.assertEquals("新しいサービスを開始します。", driver.findElement(By.tagName("body")).getText());
	}
}
```

できたら、TestMainを右クリック、Run As→JUnit Testで実行します。
ブラウザが立ち上がりテストが実行され、Eclipseの右下のJUnitビューで緑のバーが出ていれば成功です。赤いバーだと失敗。何かがうまく行っていません。本テキストを読みなおしてもう一回やってみましょう。
さて、これでコードが完成したので、コミットを行います。
変更があったファイルやディレクトリをgitの監視下に入れます。

```
git add build.gradle
git add src/main/java/org/
git add src/test/java/test/
git add test
```

コミットとプッシュを行います。

```
git commit -m “予告ページの作成"
git push origin feature/noticePage
```

あとは、GitHub上でPull Requestを作成し、レビューを受け、masterにマージを行います。
これでアプリケーションが完成しました。

## リリースイメージの作成

つづいて、リリースイメージのを行います。
リリースとは、サーバ上で作ったアプリを動かすことです。
今回作ったアプリでは、OSと、Javaのインストールと、今回作ったアプリケーションをビルドしたものが必要です。
以前はサーバとなるハードウェアを用意し、そこにOSとJavaとアプリケーションを手でインストールしました。
しかしながら、アプリケーションをアップデートするたびに手でいじっていくと、そのサーバ上で手で何をしたかがわからなくなり、
サーバの状態を再現できなくなってしまいます。それにより、開発している環境と世の中に公開する環境（よく本番環境と呼びます。）の差異が出てしまい、開発環境では動くのに本番環境では動かない、ということが発生していました。
現在はリリース作業の殆どをソフトウェア化し、何度リリースしても、どこの環境でも同じ動きをする様なやり方をします。
そして、動作させる環境に変化、たとえばJavaのバージョンを変更するなど、があったときも、環境をバージョン管理します。
今回はDockerという仕組みを使ってOSとJava、アプリケーションを一つにしたもの（イメージと言います。）を作成し、リリースを行います。
早速、Dockerでリリースするイメージを作成します。
DockerはLinuxの仮想化技術なので、VirtualBox上で動くLinuxを構築します。

```bash
docker-machine create --driver virtualbox default
```

このLinuxに接続するように環境変数を設定します。

```
eval $(docker-machine env default)
```

続いて、Dockerfileというファイルを作成し、Dockerでどのようなイメージを作成するかを記述していきます。
引き続き、プロジェクトのトップディレクトリにDockerfileという名称のファイルを作成し、以下の様に内容を記載します。

```
FROM openjdk:8-jdk-alpine

ARG PROJECT_DIR="/var/lib/MyProject" 

RUN mkdir -p $PROJECT_DIR
COPY build/libs/MyProject2.jar $PROJECT_DIR
RUN ln -s $PROJECT_DIR/MyProject2.jar /etc/init.d/MyProject2
RUN adduser -S springboot

USER springboot
ENTRYPOINT ["java", "-jar", "/var/lib/MyProject/MyProject2.jar"]
```

作成したら、トップディレクトリで以下のコマンドを実行します。

```
docker build -t myProject:1.0 .
```

-t の後ろはイメージ名で、:の後がタグ名（バージョン名）です。最後の.はDockerfileのある場所を指定しています。コマンドの実行結果として、今回作ったアプリケーションの実行環境であるDockerイメージが作成されます。
一度、自分のパソコンでイメージが動くかどうかやってみましょう。

```
docker run -p 1080:8080 -d myProject:1.0
```

-pの後がポート変換で指定するポートです。上記のコマンドだと、VirtualBox上のLinuxの1080番ポートにアクセスされたら、Dockerコンテナの8080番ポート、つまりは今回作ったあぷりに接続するように指定しています。-d の後がコンテナを作成するのに使用するDockerイメージ、つまりは今回、先に作成したイメージです。
さて、動いたかどうか、見てみましょう。まず、VirtualBox上のLinuxのIPアドレスを調べます。

```
echo $DOCKER_HOST
```

tcp://192.168.99.100:2376のようなURLが表示されたと思います。この内IPアドレスを覚えておいてください。表示されたのがtcp://192.168.99.100:2376だとすると192.168.99.100です。これがVirtualBox上のLinuxのIPアドレスです。
続いて、ブラウザでVirtualBox上のLinuxの1080番ポートに接続します。URLはIPアドレスが192.168.99.100だとhttp://192.168.99.100:1080です。
作ったサイトが表示されたら、成功です。
なお、gradleからDockerを操作する[Gradle Docker plugin](https://github.com/bmuschko/gradle-docker-plugin)というものがあります。build.gradleにプロジェクトに関する全ての設定を集約するのもいいアイデアだと思います。興味があれば使ってみるのも良いでしょう。

## リリース

いよいよ、作成したイメージをAmazon Web Services上で動かし、世界中からアクセスできるようにします。
Amazon Web Servicesとは、クラウドサービスと呼ばれるサービスのひとつです。
クラウドサービスとは、コンピューティングリソースをインターネット上で取得できるサービスのことです。コンピュータリソースとは、たとえばコンピュータであったり、たとえばデータベースソフトであったり、機械学習のソフトウェアのことです。今日ではさまざまなコンピューティングリソースが手軽に取得できます。「クラウドファースト」という言葉があるくらい、アプリケーションを公開するときはまずクラウドを使うことが有力な公開方法です。
ほかのクラウドサービスにはMicrosoft Azureや、Google Cloud Platformなどがあります。
今回はAmazon Web Services、（以下、AWS）を使います。
AWSにも、さまざまなコンピューティングリソースが用意されていますが、今回使用するのはDNSサービスであるRoute53、ロードバランサの機能であるELB、そしてDockerを動かす環境であるECSです。今後別のサービスも使っていきます。
まず、ECSに作ったDockerイメージを登録します。最初にAWSにDockerイメージ登録用のリポジトリを作成します。[こちら](https://ap-northeast-1.console.aws.amazon.com/ecs/home?region=ap-northeast-1#/repositories)にアクセスして、Create repositoryボタンを押します。Repository nameに任意の名前を入力して、//TODO
次にDockerを動作させる環境をAWSに作成します。
[こちら](https://ap-northeast-1.console.aws.amazon.com/ecs/home?region=ap-northeast-1#/firstRun) にアクセスして、Next Stepを押します。
最初にリポジトリを作るように促されます。任意の名称を入力して、Next Stepを押します。
コマンドが出てくるので、そのコマンドを打っていきます。aws ecr get-login --region ap-northeast-1を打つと、AWSのdocker imageへ登録するためのログインコマンドが表示されるので、その通りに入力します。eval $(aws ecr get-login --region ap-northeast-1)と打ってもいいでしょう。次のコマンドはDockerイメージを作成するものです。すでに作成が完了しているので、飛ばしてもいいです。先程は「myProject:1.0」というタグ名でイメージを作成しました。次のコマンド、docker tag myproject:latest 831296567805.dkr.ecr.ap-northeast-1.amazonaws.com/myproject:latestでは、イメージに新たなタグ名をつけています。Dockerでは登録するリポジトリとタグ名を関連付けることで、どこにそのイメージを登録するかを指定することができます。
最後のdocker push 831296567805.dkr.ecr.ap-northeast-1.amazonaws.com/myproject:latestで、dockerイメージをAWSのDockerリポジトリに登録しています。
イメージの登録が終わったら、AWSコンソールに戻ってNext Stepボタンを押します。
次にTaskを作るように促されます。Task definition nameには任意の名前を、myProjectにも任意の名前を入力してください。Imageには先ほど作成したリポジトリのURLが入力されていますので、そのままに。Memory Limits (MB)もそのまま。Port mappingsには
Host portに8080、Container portに8080を、ProtocolにはTCPを入力してください。
Next stepを押して、Service nameに任意の名前を入れます。Elastic load balancingで、Container name: host portの入力欄を開くと、全ページで作成したコンテナ名と8080がついた選択肢（デフォルトだとsample-app:8080）があるのでそれを選択します。、その他はそのままでNext stepを押します。
次の画面もそのままの値で、Review & launchボタンを押し、次の画面で、Launch instance & run serviceボタンを押します。
//TODO AWSが壊れてるので、回避方法を実装。→ELBのヘルスチェックが80になっているので修正
//TODO DNSでアクセスできるように。

## さらなるアプリ拡張。データベースにデータを保存する。

次はサービスが開始した際にユーザに情報を受け取ることができるように、メールアドレスを登録できるようにします。
まずは画面を作成します。
最近のWebアプリケーションでは、サーバアプリはAPIのみを提供し、ブラウザからそのAPIを利用することによってWebアプリケーションを実行します。
ブラウザからAPIを使うためにいろいろなライブラリ、フレームワークがありますが、今回はAngularJSというフレームワークを使用します。
この手のライブラリ、フレームワークにはほかにReactなど、凄まじくたくさんのフレームワークがあります。調べてみても良いかもしれません。
アプリからAngurarJSを利用可能なようにbuild.gradleに
メールアドレスをシステムで保持しておくために、データベースというソフトウェアを使用します。
データベースとは、データを保持することができるソフトウェアのことで、リレーショナル・データベースとそうでない、NoSQLデータベースと言うものがあります。今回はリレーショナルデータベースの製品の一つであるPostgreSQLを使用します。リレーショナル・データベースには他にも有料の製品となるOracleや、オープンソースのMariaDBなどがあります。
Dockerを使用して、PostgreSQLを起動します。
```
docker run --name postgres -d -p 5432:5432 postgres
```
さらに、PostgreSQLにデータの格納場所を作成します。
```
docker exec -it postgres createdb -U postgres mydb
```
せっかくなので、この手順をbuild.gradleに記入し、次回からは簡単に実行できるようにします。以下をbuild.gradleの一番下に追記してください。

```groovy
task initPostgres() {
	"docker rm -f postgres".execute().waitFor();
	"docker run --name postgres -d -p 5432:5432 postgres".execute().waitFor();
	"sleep 10".execute().waitFor();
	"docker exec -it postgres createdb -U postgres mydb".execute().waitFor();
}
```

パソコンを再起動した場合等は、
```
gradle initPostgres
```
と打つことでPostgresを起動することができます。このように、実行しなければならないコマンドや作業をbuild.gradleに追記していくことで開発をIT化していってください。build.gradleが優秀になればなるほど、作業が効率化されます。
次にGradleにDB接続用のライブラリへの依存性を追加します。
build.gradleのdependenciesに
compile('org.springframework.boot:spring-boot-starter-data-jpa')
runtime('org.postgresql:postgresql')
を追加します。
続いて、gradle eclipseを実行して//TODO Gradle IDEを使う。
データベースへアクセスするアプリケーションを作成します。
今回はSpring JPAという仕組みを利用してデータベースにアクセスしします。
通常、RDBを使う場合はDDL（Data Definition Language）という記法を使って、データの保存形式を定義し、そこにデータを入れていくのですが、Spring JPAはデータ型をJavaのプログラム定義さえすれば勝手にデータの保存形式を作成してくれるのでとても楽です。
データ定義は以下の様に記述します。

```java
package org.my.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Reservation {
    @Id
    @GeneratedValue
    public long id;

    @Column(unique = true)
    public String mail;

    @Column
    public boolean sended;

    @Temporal(TemporalType.TIMESTAMP)
    public Date createdAt;

    @PrePersist
    private void onCreate() {
        this.createdAt = new Date();
    }
}
```

@Entityをつけることで、このクラスはデータベースに保存するためのクラスであることを定義しています。@Id @GeneratedValueをつけると勝手にユニークな数値を入れてデータベースに保存してくれます。大抵のデータベースに保存するクラスにはこのクラスのようにidを定義しておくと良いでしょう。 @Column(unique = true) では、重複したメールアドレスを登録されないようにしています。これに違反するデータを保存しようとすると、例外が発生します。@Temporal(TemporalType.TIMESTAMP)はTIMESTAMP型で日付データを保存することを指定しています。時間や日付をデータベースに保存したい場合は、大抵はTIMESTAMP型にしておくと良いです。
また、@PrePersitをつけたメソッドは保存直前に実行されます。ココではcreatedAtに今の時間が常に入るようにしています。

つづいて、データを保存・取得するためのクラスを定義します。Spring JPAのリポジトリと呼ばれるクラスです。

```java
package org.my;

import org.my.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {
    public Reservation findByMail(String name);
}

```

このクラスを介してデータベースにデータを保存・取得を行います。
Spring JPAのリポジトリはメソッド名にfindByXXXXという名前をつけておくと、XXXXの値を受け取ってそれを検索キーにしてデータの検索をおこなう処理を勝手に作ってくれるのでとても便利です。
さて、続いてはブラウザからの命令によってデータを保持するサーバのAPIを用意します。
今回はRESTというルールにそってサーバのAPIを作成していきます。

```java
package org.my.controller;

import javax.validation.Valid;
import org.my.ReservationRepository;
import org.my.dto.ReservationDto;
import org.my.entity.Reservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/reserve")
public class ReservationController {

    @Autowired
    ReservationRepository repository;

    @RequestMapping(method = RequestMethod.POST)
    public void reserve(@Valid @RequestBody ReservationDto reserve) {
        Reservation reservation = new Reservation();
        reservation.mail = reserve.mail;
        repository.save(reservation);
    }
}
```

@RestControllerをクラスにつけることで、このクラスは外部から呼び出せることを宣言しています。また、@RequestMapping("/api/reserve”)では、このクラスはURLが/api/reserveで呼び出せることを示しています。
@Autowired
ReservationRepository repository;
で、先ほど作成したリポジトリを使えるようにしています。@Autowiredをつけておくと、Spring Frameworkがこのインスタンス変数を使えるように初期化してくれます。
メソッドに付いている@RequestMapping(method = RequestMethod.POST)は、HTTPのPOST通信でこのメソッドが実行できることを示しています。HTTPにはGET,POST,PUT,DELETEの４パターンのアクセスがあります。RESTは、特定のURLにこの4パターンの処理を作り、GETでは取得を、POSTではデータ生成、PUTではデータのアップデート、DELETEではデータの削除を行うように設計します。
ここでは、/api/reserveにPOST通信がくると、予約情報を保存するようにしています。保存する処理がpublic void reserve(@Valid @RequestBody Reserve reserve)に記載されています。
おっと。ReservationDtoについて説明するのを忘れていました。
ブラウザからのアクセスはこのクラスに詰められてきます。ReservationDtoは以下の様に定義しています。

```java
package org.my.dto;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Email;

public class ReservationDto {
    @Email
    @NotNull
    public String mail;
}
```

インスタンス変数にアノテーションを定義し、Emailの形式（@がある）であること、Nullではないことを定義しています。これに違反するデータをAPIに与えると例外が発生します。例外とは、Javaの決まりで、処理の途中で意図しない状態になったときにプログラムが通常とは異なる動きに鳴ることです。後ほど詳しく説明します。

ReservationControllerに説明を戻します。reserveメソッドでデータを保存しています。Reservation reservation = new Reservation();で保存用のオブジェクトをインスタンス化し、reservation.mail = reserve.mail;で、APIに投入された情報を保存用のインスタンスに移し替え、repository.save(reservation);でデータを保存しています。
ここで気づくことがあります。repositoryはReservationRepositoryクラスのインスタンスなんですが、ReservationRepositoryにデータを保存する処理であるsaveは作成していないにも関わらず使えています。これは実はSpringが勝手に作ってくれています。便利ですね。
これにより、RESTで通信ができるようになりました。
最後にDBに接続する設定をかきます。
src/main/resourcesの下に、configというディレクトリを作成し、その中にapplication.ymlというファイルを作成します。内容は以下のようにしてください。

```yml
spring:
  datasource:
    url:  jdbc:postgresql://192.168.99.100/mydb
    username: postgres
    password: postgres
    driverClassName: org.postgresql.Driver
  jpa:
    database-platform: org.hibernate.dialect.PostgreSQLDialect
    show-sql: false
    hibernate:
      ddl-auto: create
```

このファイルの形式はYAMLといって、XMLよりも簡単に構造情報を記載する事ができます。spring.datasource.urlのIPアドレスははDockerMachineのIPにしてください。DockerMachineのIPは以下のコマンドを実行し、
docker-machine env default
表示されるDOCKER_HOSTのIP部分です。
```
$ docker-machine env default
export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://192.168.99.100:2376"
export DOCKER_CERT_PATH="/Users/tito/.docker/machine/machines/default"
export DOCKER_MACHINE_NAME="default"
## Run this command to configure your shell:
## eval $(docker-machine env default)
```
上記例だと「192.168.99.100」がIPアドレスです。

引き続いて、このAPIを呼び出すブラウザのアプリケーションを作成していきます。
ところで、Webアプリケーションの仕組みについて、ここで説明します。Webアプリケーションとは、サーバと呼ぶコンピュータと、クライアントと呼ぶコンピュータの２つを使って動作するアプリケーションです。今までJavaで作っていたのがサーバで動くアプリケーションで、今から作るのがクライアントで動くアプリケーションです。Webアプリケーションはサーバとクライアントのアプリケーションが通信し合うことで動作します。クライアントにはブラウザと呼ばれるソフトウェアがインストールされている必要があり、クライアントで動くアプリケーションはブラウザ上で動作します。ブラウザとは、Chromeや、Firefox、SafariやIE（インターネットエクスプローラ）のことで、現在ほぼすべてのパソコン、スマートフォンにインストールされています。つまり、ブラウザ上で動作するアプリケーションを作ることでほぼすべてのパソコン、スマートフォンで動作するアプリケーションを提供することが可能になるのです。
さて、今からクライアントのブラウザ上で動作するアプリケーションを作っていきます。
ここでは、AngularJSとTwitter Bootstrapというソフトウェアを利用します。この２つを使うことで、楽にきれいなアプリケーションを作ることができます。
この２つのライブラリを使えるように、build.gradleに追加記述を行います。
dependenciesの中に以下の三行を追記します。

```
compile 'org.webjars:jquery-ui:1.11.4'
compile 'org.webjars:bootstrap:3.3.7-1'
compile 'org.webjars:angularjs:1.5.8'
```

Eclipseのプロジェクトを右クリックし、Gradle(STS)→Refresh Allを選択します。もし、MyApplicationを実行中であったら、一度停止し、再起動をしてください。
この3つのライブラリはWebjarsと呼ばれる。ライブラリです。WebjarsとはJarと呼ばれるJava用のライブラリを入れるファイルにJavaScriptなどのファイルを入れてアクセスができるようにしたものです。アプリケーションが使っているライブラリをすべてGradleで一元管理すると便利なので、今回はこのWebjarsを利用しています。
src/main/resources/index.htmlは、実はブラウザ上にダウンロードされて動くアプリケーションです。index.htmlに改造を加えて、いろいろなことをできるアプリケーションにしていきます。
まず、index.htmlでJavaScriptを読み込みます。index.htmlに以下の行を追加してください。

```
<script src="/webjars/jquery/1.11.1/jquery.min.js"></script>
<script src="/webjars/angularjs/1.5.8/angular.min.js"></script>
<script src="/webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="/webjars/bootstrap/3.3.7-1/css/bootstrap.css"></link>
```

これでライブラリを読み込めるようになりました。
つづいては、ヘッダーを作成します。
&lt;body&gt;タグの直下に

```html
<nav class="navbar navbar-default">
        <div class="col-xs-12">
            <a href="/"><img src="image/title.png" style="height: 50px” /></a>
        </div>
</nav>
```
と記載します。この<と>で囲まれた表現はHTMLといいます。<タグ名></タグ名>のような表現を入れ子にして、<タグ名 情報キー=“情報値"></タグ名>のように、始まりのタグには情報を追記することができます。HTMLには様々なタグや表現方法があるので、必要に応じて学んでみるといいでしょう。//TODO オススメHTML本
さて、ヘッダー部分はおもにTwitter Bootstrapを利用して作成しています。
Twitter BootStrapはHTMLを便利に書けるライブラリで、様々なCSSクラスが定義されています。navbarやcol-xs-12などはそこに定義されているクラスです。
ここで大事なのが「col-xs-12」です。Twitter Bootstrapでは、画面を横に１２分割し、そのうちいくつを使うかを指定することができます。col-xs-1〜col-xs-12まであり、最後の数字部分で横幅を決めています。例えば画面を横に２分割したい場合、
```html
<div class=“col-xs-6”></div>
<div class=“col-xs-6”></div>
```
とすると横２分割になります。また、「col-xs-12」のxsの部分は画面サイズを示しています。小さい順にxs、sm、md、lgとあります。この画面サイズと横幅の組み合わせで小さなスマートフォンの画面と大きなデスクトップの画面と同じHTMLで記載することができます。例えば、
```html
<div class=“col-xs-12 col-lg-6”></div>
<div class=“col-xs-12 col-lg-6”></div>
```
とすると、小さな画面では画面横いっぱいで、縦に２列になり、大きな画面では横に２分割になります。このデザイン方法のことをレスポンシブウェブデザインと言います。
詳しくはhttp://getbootstrap.com/css/#grid を読んでみてください。
ところで、上記のHTMLの中で&lt;img src="image/title.png" style="height: 50px” /&gt;とあります。ここでは画像を読み込んでいます。好きな画像を用意し、src/main/resources/image/title.pngに配置して、ヘッダの一部として読み込んでみましょう。画像はなんでも構いません。私はOpenOfficeの図形描画を利用して作成しました。
さて、作成した画面を確認してみましょう。
http://localhost:8080 にアクセスしてみてください。
![](/images/new_service.png)

こんな風にヘッダが表示されていれば正解です。
続いて、入力欄を作ります。
先ほどの</nav>の下には、最初に記載した「新しいサービスを開始します。」という言葉があると思いますが、その下にメールの入力欄を作ります。下のタグを追加してみてください。
```html
<input type=“email” />
```

![](/images/new_service.png)
四角ができました。これがメールの入力欄です。
入力欄だけだとよくわからないので、説明を追加します。
```html
新しいサービスを開始します。
<div>メールを入力していただければ、サービス開始時にお知らせをします。</div>
<input type="email" />
```
&lt;div&gt;&lt;/div&gt;のタグでくくっているのはなんとなくです。
忘れていました。せっかくなのでTwitterBootstrapのタグで、説明文全体を囲んでみましょう。ついでに「新しいサービスを開始します。」もタグで囲みます。
```html
<div class="col-xs-12">
	<div>新しいサービスを開始します。</div>
	<div>メールを入力していただければ、サービス開始時にお知らせをします。</div>
	<input type="email" />
</div>
```
![](/images/new_service_app.png)
こんな風に実際の開発でも、出来上がりを確認しながら、少しづつ作っていくのが良いでしょう。
引き続いて、メールを入力した後に送信するボタンを作成します。
```html
<div class="col-xs-12">
	<div>新しいサービスを開始します。</div>
	<div>メールを入力していただければ、サービス開始時にお知らせをします。</div>
	<input type="email" />
</div>
<button type="button">送信</button>
```
![](/images/new_service_app_button.png)

せっかくTwitter Bootstrapを使っているので、入力欄とボタンをかっこよくします。
```html
<div class="col-xs-12">
	<div>新しいサービスを開始します。</div>
	<div>メールを入力していただければ、サービス開始時にお知らせをします。</div>
	<div class="input-group col-sm-4">
		<input type="email" class="form-control" /> 
		<span class="input-group-btn">
			<button type="button" class="btn btn-primary">送信</button>
		</span>
	</div>
</div>
```
![](/images/new_service_app_boot.png)

かっこよくなりました。そしてHTMLはちょっと複雑になりましたね。
このあたりのBootStrapの使い方は[Twitter BootstrapのWebページ](http://getbootstrap.com/)にまとまっています。
これで基本的な画面は出来上がりです。引き続いてこの画面に動きをつけていきます。
動きはJavaScriptという言語を使ってつけます。簡単に動きをつけられるようにAngular.JSというライブラリを使います。
Angular.JSはHTMLを拡張し、JavaScriptで操作しやすくするライブラリです。
まず、src/resources/static/jsというフォルダを作成し、その中にapp.jsというファイルを作成してください。JavaScriptはHTMLの中に埋め込むこともできますが、大体は単独のファイルとして作成し、それをHTMLから読み込みます。HTMLに埋め込んでみましょう。
index.htmlに以下の行を追加してください。
```html
<script src="/js/app.js"></script>
```
これで、index.htmlをブラウザで表示したときにapp.jsの中に書いてあるJavaScriptが実行されるようになりました。
さて、app.jsにJavaScriptを記述していきます。
```javascript
var app = angular.module("app", []);
var reserveController = function(){}
app.controller("reserveController", reserveController);
```
Angular.JSではモジュールとコントローラという考え方を用います。
モジュールを使うと、Angular.JSに機能を追加することができます。
コントローラは画面に動きを追加するときに使用します。
var app = angular.module('app', []);
で画面操作するための新しい機能をAngular.JSに追加しています。
var mainController = function(){}
でコントローラを作成し、
app.controller('mainController', mainController);
でコントローラをモジュールに追加しています。
これにより、Angular.JSに画面操作の機能を追加しています。
引き続いて、HTMLに操作を加えていきます。
index.htmlの最初に&lt;html&gt;があります。それを編集します。
```html
<html ng-app="app" ng-controller="mainController">
```
作成したappというモジュールとreserveControllerというコントローラを
htmlタグで使えるようにします。
後は、入力されたメールをControllerで受け取れるようにします。
index.htmlの&lt;input type="email" /&gt;にattributeを追加します。
```html
<input type="email" class="form-control" ng-model="mailAddress" /> 
```
これで、ng-modelに設定されたmailAddressに値が入るようになります。
実際に値が入っているか、確認のために一時的にタグを入れてみましょう。&lt;/body&gt;の直前に以下のタグを入れてみてください。
```html
<div>あなたのメールアドレスは<span ng-bind="mailAddress"></span>です。</div>
```
ng-bind="mailAddress"を指定することでそのタグの中身にmailAddressが入るようにしています。
それでは、http://localhost:8080を表示してみてください。すでに表示されている場合はF5やリフレッシュボタンを押して、画面を更新してください。
入力欄にメールアドレスを入れると
![](/images/put_mailaddress.png)
のように、画面にメールアドレスが表示されるようになっています。
これはAngular.JSがinputタグの内容をmailAddressという変数に入れ、その後にspanの中に書き出してくれているためです。
さて、続きまして、ボタンが押されたらmailAddressをサーバに投稿する処理を追加していきます。
サーバに追加する処理はapp.jsに書いていきます。
まず、controllerで、mailAddressを受け取れるようにする機能と通信を行う機能を使えるようにします。
```javascript
var app = angular.module("app", []);
var reserveController = function($http, $scope){}
app.controller("reserveController", ["$http", "$scope", reserveController]);
```
続けて、ボタンを押されたときに処理が動くようにします。app.jsを以下のように改造してください。
```javascript
var app = angular.module("app", []);
var reserveController = function($http, $scope){
　　　　$scope.sendButtonClicked = function(){
　　　　    alert("メールアドレスは" + $scope.mailAddress + "です。");
　　　　}
}
app.controller("reserveController", ["$http", "$scope", reserveController]);
```
ボタンが押されたときに処理が動くか、また、mailAddressが受け取れているかを確認するために、javascriptのalert()の機能を使ってダイアログが表示されるようにしてみました。
続いて、index.htmlのbuttonタグを以下の様に改造します。
```html
<button type="button" class="btn btn-primary" ng-click="sendButtonClicked()">送信</button>
```
ブラウザを更新して、メールアドレスを入力して、送信ボタンを押してください。
![](/images/mail_dialog.png)
ダイアログが表示され、メールアドレスが出力されていたら成功です。
alart()の部分はすぐに消してしまうのですが、これでボタンを押したらsendButtonClicked()が呼ばれることが確認できました。
このように、作った部分が動くことを確認しながら、少しづつ少しづつ完成した部分を積み重ねていくのがコツです。

引き続いて、サーバに通信を行います。app.jsを以下の様に改造してください。
```javascript
var app = angular.module("app", []);
var reserveController = function($http, $scope){
　　　　$scope.sendButtonClicked = function(){
　　　　    $http.post("/api/reserve", {
			mail : $scope.mailAddress
		   }).then(function(response) {
			   $scope.reserved = true;
		   }, function(response) {
			    $scope.unknownError = true;
		   });
　　　　}
}
app.controller("reserveController", ["$http", "$scope", reserveController]);
```
$http.post(url, data).then(success, fail) を使うと、サーバに通信を行うことができます。
成功したら入力欄が消えメールアドレスが送信されたことを示すメッセージを表示し、失敗したら「何か問題が発生しました。」と表示するようにindex.htmlを改造します。
```html
<div class="col-xs-12">
	<div>新しいサービスを開始します。</div>
	<div>メールを入力していただければ、サービス開始時にお知らせをします。</div>
	<div class="input-group col-sm-4" ng-hide="registed">
		<input type="email" class="form-control" ng-model="mailAddress" /> <span class="input-group-btn">
			<button type="button" class="btn btn-primary" ng-click="sendButtonClicked()">送信</button>
		</span>
		<div id="unknownErrorArea" class="has-error" ng-show="unknownError">
				<label class="control-label">何か問題が発生しました。</label>
		</div>
	</div>
	<div class="col-sm-4" ng-show="registed">メールアドレスを登録しました。:<span ng-bind="mailAddress"></span></div>
</div>
```
ここでのポイントはng-hide="registed"とng-show="registed"です。ng-hideは、=の後の値がtrueだったらそのタグを表示しない、 ng-showは=の後の値がtrueのときにのみ表示する。つまりfalseだと表示しないというものです。 ng-hide="registed"のregistedはapp.jsにあった、$scope.registedと同じものです。$scope.registedがtrueであれば、ng-hideは非表示、ng-showは表示され、$scope.registedがfalseであれば、ng-hideは表示、ng-showは非表示になります。つまり、app.jsでメールの登録が成功すれば入力欄が非表示になり、「メールアドレスを登録しました。」のメッセージが表示されることになります。
さて、ブラウザを更新し、動かしてみましょう。
![](/images/angular_hidden.png)
このように入力欄が消えて登録完了のメッセージが表示されれば成功です。
DBに登録されているかも確認してみましょう。

```
$docker exec -it postgres /bin/bash
$psql -U postgres mydb
psql (9.6.0)
Type "help" for help.
mydb=## select * from reservation;
 id |       created_at        |        mail        | sended 
----+-------------------------+--------------------+--------
  1 | 2016-10-17 20:55:36.569 | ukiuni@example.com | f
(5 rows)
mydb=## 
```
docker execで、Dockerコンテナ上でコマンドを実行することができます。/bin/bashを実行することでコマンドプロンプトに入れます。psqlはPostgreSQLを操作するためのコマンドです。「select * from reservation;」はSQLと呼ばれる言語です。DBの検索をすることができます。
その後に表示されているのが、実際に登録された情報です。メールアドレスが登録されていることが確認できますね。
アプリができていることが確認できたので、テストを書いてみましょう。
ところで、最初に書いたテスト、TestMainを動かしてみましょう。テストが通らなくなっていませんか？
```
org.junit.ComparisonFailure: expected:<新しいサービスを開始します。[]> but was:<新しいサービスを開始します。[
メールを入力していただければ、サービス開始時にお知らせをします。
```
こんなメッセージが出てます。それもそのはず、最初は画面に表示されるのは「新しいサービスを開始します。」だけだったのが、色々追加したからです。このテストが通るようにメンテナンスしましょう。
「新しいサービスを開始します。」の部分をdivで囲い、idをつけます。
```html
<div id="read">新しいサービスを開始します。</div>
```
さらに、TestMainの文字を抜き出す部分をカスタマイズします。
```java
Assert.assertEquals("新しいサービスを開始します。", driver.findElement(By.tagName("body")).getText());
```
の部分を
```java
Assert.assertEquals("新しいサービスを開始します。", driver.findElement(By.id("read")).getText());
```
By.id("read")とすることで、id="read"となっている部分の中身のテキストを取得できます。これでテストが通るようになりました。実行してみましょう。
![](/images/put_mail_test_pass.png)
成功しました。このようにテストをメンテナンスし続けることで、ソフトウェアが動くことを担保し続けることが重要です。
今度は新しく作った機能である、メール登録機能を確認してみましょう。TestMainに新しいメソッドを一つ追加します。

```java
@Test
public void testRegistMail() {
	driver.get("http://localhost:9999");
	String inputMail = new Date().getTime() + "ukiuni@example.com";
	driver.findElement(By.tagName("input")).sendKeys(inputMail);
	driver.findElement(By.tagName("button")).click();
	WebDriverWait wait = new WebDriverWait(driver, 5);
	wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("registResult"), "メールアドレスを登録しました。"));
	Assert.assertEquals("メールアドレスを登録しました。:" + inputMail, driver.findElement(By.id("registResult")).getText());
}
```
登録後のメッセージが確認しやすいように、index.htmlのメッセージのタグにIDをつけておきます。
```html
<div ng-show="registed">
	メールアドレスを登録しました。:<span ng-bind="mailAddress"></span>
</div>
```
を
```html
<div ng-show="registed" id="registResult">
	メールアドレスを登録しました。:<span ng-bind="mailAddress"></span>
</div>
```
にします。
このテストでメールアドレスを入力したらメッセージが表示されることが確認できます。
これで、メールの登録処理が動くことが確認できました。そして、ソフトウェアを開発していく上で大事なことが、どのような使われ方をするかを予め想定し、網羅しておくことです。
例えば、入力欄に何も入れずに、送信ボタンを押したらどうなるでしょう？または、同じメールアドレスが２回入力されたらどうなるべきでしょう？メールアドレスではない文字が入力されたら？これらも想定した上でソフトウェアを作っていきます。
実際に入力欄を空文字にして、ボタンを押してみましょう。何もおきませんね。ここは、メールアドレスが入力されていない状態ではボタンは押せないようにするのが使いやすいと思います。入力欄に何も入力せずに送信ボタンを押したときのテストを作ってみます。TestMainに新しいメソッドを追加します。

```java
@Test
public void testEmptyInput() {
	driver.get("http://localhost:9999");
	Assert.assertFalse("初期状態ではボタンは押せないこと", driver.findElement(By.tagName("button")).isEnabled());
}
```
初期状態ではボタンは有効になっていない状態が正しいとするテストです。実行してみます。
![](/images/test_fail_on_escape.png)
失敗しますね。今は初期状態でもボタンが押せるからです。
さて、入力欄が空であった場合、ボタンを無効化してみましょう。index.htmlを編集します。

```html index.html(抜粋)
<div>メールを入力していただければ、サービス開始時にお知らせをします。</div>
<div class="input-group col-sm-4" ng-hide="registed">
	<form name="form">
		<input type="email" class="form-control" name="mail" ng-model="mailAddress" required />
	</form>
	<span class="input-group-btn">
		<button type="button" class="btn btn-primary" ng-click="sendButtonClicked()" ng-disabled="form.mail.$error.required">送信</button>
	</span>
</div>
```

入力エリアを&lt;form name="form"&gt;&lt;/form&gt;で囲み、さらにinputのattributeにname="email"とrequiredを増やしました。これはTwitter Bootstrapの入力確認機能、Validation機能を使うためです。
そして、buttonにng-disabled="form.mail.\$error.required"を追加しました。ng-disabled="値"は値があればボタンを無効にしてくれます。値の中身は、「formタグのname.inputタグのname.\$error.確認内容」のフォーマットになっています。
さて、テストを動かしてみましょう。
![](/images/success_test_after_empty_put.png)
成功しました。
これで、入力エリアが空であった場合にはボタンが押せなくなりました。
入力欄はこれ以外にも、メールアドレス以外が入力される可能性があります。その時もボタンを有効化したくないですし、エラーメッセージを表示するのが親切だと考えます。メールアドレス以外が入れられたときのテストを追加してみましょう。
```java TestMain.java（抜粋）
@Test
public void testNotEmailInput() {
	driver.get("http://localhost:9999");
	driver.findElement(By.tagName("input")).sendKeys("notemail");
	Assert.assertFalse("メールアドレスでない入力ではボタンは押せないこと", driver.findElement(By.tagName("button")).isEnabled());
}
```

![](/images/test_fail_with_wrong_mail.png)

index.htmlを改造します。
```html index.html(抜粋)
<div>メールを入力していただければ、サービス開始時にお知らせをします。</div>
<div class="input-group col-sm-4" ng-hide="registed">
	<form name="form">
		<input type="email" class="form-control" name="mail" ng-model="mailAddress" required />
	</form>
	<span class="input-group-btn">
		<button type="button" class="btn btn-primary" ng-click="sendButtonClicked()" ng-disabled="form.mail.$error.required ">送信</button>
	</span>
</div>
<div class="col-sm-4">
	<div id="wrongMailAddress" class="has-error" ng-show="form.mail.$error.email">
		<label class="control-label">正しいメールアドレスを入力してください。</label>
	</div>
</div>
```
ng-disabled="form.mail.\$error.required || form.mail.\$error.email"と、メールのValidationが追加されているのと、入力エラーの際の表示である
```html index.html(抜粋)
<div class="col-sm-4">
	<div id="wrongMailAddress" class="has-error" ng-show="form.mail.$error.email">
		<label class="control-label">正しいメールアドレスを入力してください。</label>
	</div>
</div>
```
が追加されています。テストに入力エラーの際の表示が出ることが漏れていたので、追加します。
```java TestMain.java（抜粋）
@Test
public void testNotEmailInput() {
	driver.get("http://localhost:9999");
	driver.findElement(By.tagName("input")).sendKeys("notemail");
	Assert.assertFalse("メールアドレスでない入力ではボタンは押せないこと", driver.findElement(By.tagName("button")).isEnabled());
	Assert.assertTrue("メールアドレスエラーが表示されていること", driver.findElement(By.id("wrongMailAddress")).isDisplayed());
}
```

これで入力エラーが表示できるようになりました。
さらに、メールアドレスの重複エラーを確認してみましょう。
テストを書いてみます。

```java TestMain.java（抜粋）
@Test
public void testDuplicateEmailInput() {
	String inputMail = "dupilcate@example.com";
	for (int i = 0; i < 2; i++) {
		driver.get("http://localhost:9999");
		driver.findElement(By.tagName("input")).sendKeys("notemail");
		driver.findElement(By.tagName("input")).sendKeys(inputMail);
		driver.findElement(By.tagName("button")).click();
	}
	WebDriverWait wait = new WebDriverWait(driver, 5);
	wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("mailDuplicateErrorArea")));
	Assert.assertTrue("重複エラーが表示されていること", driver.findElement(By.id("mailDuplicateErrorArea")).isDisplayed());
}
```

２回、同じメールを入力したらエラーになるようにします。
もちろん、まだこのテストは通りません。このテストが通るようにアプリを改造していきます。しかしながら、実は、今まで作ってきた内容で、メールは重複して登録されないようになっています。Reservation.javaを見てみましょう。
```java Reservation.java
package org.my.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
public class Reservation {
	@Id
	@GeneratedValue
	public long id;

	@Column(unique = true)
	public String mail;

	@Column
	public boolean sended;

	@Temporal(TemporalType.TIMESTAMP)
	public Date createdAt;

	@PrePersist
	private void onCreate() {
		this.createdAt = new Date();
	}
}
```

キモはpublic String mailについているアノテーション、@Column(unique = true)です。これはデータベースにデータを保存するツールであるSpring Data JPAの機能で、これによりデータベースには同じメールアドレスが重複して登録されないようになっています。
もし、重複したメールアドレスを登録しようとした場合、DataIntegrityViolationExceptionというExceptionが発生します。このExceptionが発生したら、通信で判別可能なようにします。新しいクラスを作成してください。

```java ExceptionAdvice.java
package org.my;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionAdvice {

	@ExceptionHandler
	@ResponseStatus(value = HttpStatus.CONFLICT)
	public void handle(DataIntegrityViolationException e) {
	}
}
```
このクラスを作っておくと、DataIntegrityViolationExceptionが発生した際にはSpring BootがHTTP Status 409を返却してくれます。HTTP Statusとは、通信時にその通信の処理が成功したか失敗したかを番号で通知する仕組みです。400番台は失敗で、409はConflict、つまりは処理が衝突した場合に返却される番号です。HTTP Statusについては詳しくは[こちら](https://ja.wikipedia.org/wiki/HTTP%E3%82%B9%E3%83%86%E3%83%BC%E3%82%BF%E3%82%B9%E3%82%B3%E3%83%BC%E3%83%89)を参考にしてください。
さて、通信で409エラーが返るようになったので、ブラウザの通信処理で画面にエラーを表示してやるようにします。ブラウザの通信処理はapp.jsで行っています。
```javascript app.js
var app = angular.module("app", []);
var reserveController = function($http, $scope) {
	$scope.sendButtonClicked = function() {
		$http.post("/api/reserve", {
			mail : $scope.mailAddress
		}).then(function(response) {
			$scope.registed = true;
		}, function(response) {
			if ("409" == response.status) {
				$scope.mailDuplicated = true;
			} else {
				$scope.someError = true;
			}
		});
	}
}
app.controller("reserveController", [ "$http", "$scope", reserveController ]);
```
通信時にエラーが発生したときのfunctionで、response.statusを確認し、409だったら$scope.mailDuplicatedをtrueにします。
$scope.mailDuplicatedがtrueになった場合にindex.htmlがエラーを表示するように改造します。
```html index.html(抜粋)
<div class="col-sm-4">
	<div id="unknownErrorArea" class="has-error" ng-show="unknownError">
		<label class="control-label">何か問題が発生しました。</label>
	</div>
	<div class="has-error" ng-show="form.mail.$error.email">
		<label class="control-label">正しいメールアドレスを入力してください。</label>
	</div>
	<div id="mailDuplicateErrorArea" class="has-error" ng-show="mailDuplicated">
		<label class="control-label">すでに登録されているメールアドレスです。</label>
	</div>
</div>
```
さて、これでテストが通るようになりました。
これで、メールアドレスをお客様に登録していただけるようになりました。
他にもメールアドレスを登録しやすくなる工夫があるかもしれません。気づいた場合はチケット化して実施してみましょう。

作成が終わったら、すぐリリースです。
今回、アプリケーションがデータベースを使用するようになりました。AWSではデータベースは[RDS](https://aws.amazon.com/jp/rds/)をつかうと便利です。簡単にバックアップなどを取ってくれます。RDSを作成します。
AWSマネジメントコンソールにアクセスし、RDSを選択します。
![](/images/rds_create_1.png)

エンジンにはPostgreSQLを選択します。
![](/images/rds_create_2.png)

本番稼動用か、開発/テストかを選べます。
![](/images/rds_create_3.png)
本番稼動用だと、多重化（サーバが一台壊れても、別のサーバが代わりに仕事をしてくれる）や性能の良いマシンが使えます。本気度に応じて本番稼動用か開発/テストかを選択してください。選択肢により次の画面でデフォルトで選択されている値が替わります。
本気だと
![](/images/rds_create_4.png)

お試しだと、
![](/images/rds_create_5.png)

くらいの選択にします。
次に接続設定です。任意の値を入れてください。
![](/images/rds_create_6.png)
パスワードは簡単なもので構いません。というのも、システムを運用する上で、パスワードに頼ったセキュリティーは行ってはなりません。ネットワーク的に切り離された場所で管理するか、十分な長さを持った公開鍵暗号でセキュリティーは担保してください。パスワードは相当の長さにしない限り、攻撃により破られると考えたほうが良いでしょう。パスワードが許されるのは利用ユーザのみです。今回はネットワーク的にインターネットとは切り離された場所にDBを設置します。設定は次のページです。

![](/images/rds_create_7.png)
VPCにはECSで作成したVPCを選択してください。ELBかEC2が属しているVPCがそうです。パブリックアクセス可能に「いいえ」を選択することでインターネットから直接はアクセス出来ないようにします。これによりデータベースを攻撃される危険に対処します。
![](/images/rds_create_8.png)

![](/images/rds_create_9.png)

残りは、良い値を選択してください。
これでデータベースが作成されました。
データベースのURLは、「エンドポイント」として記載されています。
![](/images/rds_create_10.png)
DBが作成されたら、RDSのセキュリティーグループを編集し、ECSコンテナのセキュリティーグループからアクセスできるようにしてください。RDSのインスタンス一覧から作成したRDSインスタンスを選択し、虫眼鏡マークのタブを押して、下の方にスクロールするとセキュリティーグループのリンクが出るので、リンクをおして次の画面で編集ボタンを押します。
![](/images/rds_create_11.png)
ECSコンテナのセキュリティーグループは、EC2インスタンス一覧から確認できます。
これで、リリース環境のDBが作成できました。//TODO つながり
普段開発で使用しているデータベースとリリース後のデータベースのURLが違うものになりました。リリースのたびにいちいち設定ファイルを書き換えるのも面倒ですし、リリースミスが起こるかもしれません。このようなときはSpring Bootの機能を利用して、本番環境ではDBの設定を上書きできるようにしましょう。
src/main/resources/configにapplication-production.ymlというファイルを作成します。
```yml application-production.yml
spring:
  datasource:
    url:  jdbc:postgresql://(作成したRDSのエンドポイント)/(テーブルスペース名)
    username: postgres
    password: postgres
```
urlの(作成したRDSのエンドポイント)と(テーブルスペース名)には、RDSを作ったときの値を入れてください。
を入れてください。usernameとpasswordも、RDS作成時に入れたものを指定してください。
このファイルがあることで、環境変数SPRING_PROFILES_ACTIVEにproductionと設定すると、通常のapplication.ymlと併せてapplication-production.ymlがロードされるようになります。同じ項目がapplication.ymlとapplication-production.ymlにあった場合にはapplication-production.ymlの内容が反映されます。
次に、build.gradleのDocker作成タスクの新しいのもを作成します。build.gradleの最後に以下を追記してください。
```groovy build.gradle(抜粋)
task buildDockerProduction(dependsOn: build) {
	doLast{
		"docker build -t my:3.12 --build-arg ENVIRONMENT=production .".execute().waitFor();
	}
}
```
さらに、DockerfileのENTRYPOINTの行の前に以下の行を追加します。
```dockerfile Dockerfile(抜粋)
ENV SPRING_PROFILES_ACTIVE ${ENVIRONMENT}
```
これでgradle buildDockerProductionした際には本番環境用のDockerイメージが作成されるようになりました。

RDSのエンドポイントを。。。。。。

