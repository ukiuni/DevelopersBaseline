# データベースへの登録

現在の状態だとデータベースにデータは保存されているものの、再起動したら消えてしまいます。現在はH2というデータベースを使用するようになっていますが、今の設定だと、メモリ上にしか保存されていないためです。
消えないデータベースにデータを保存するように設定する必要があります。

今回もGitのブランチを切ります。

```sh
git checkout master
git pull origin master
git checkout -b feature/registdb
```

## ローカルDBの作成

まず、自分のマシン上、すなわちローカルにデータベースを作成します。今回使用するのは[PostgreSQL](https://www.postgresql.org/)というデータベースです。
PostgreSQLは様々な環境にインストールすることが可能なのですが、今回は[Docker](https://www.docker.com/)という仕組みを用いてPostgreSQLをローカルに構築します。
Dockerとは、コンテナ仮想化技術と呼ばれるもので、アプリケーションを起動するための専用領域をコンピュータ上に作り出すものです。
コンピュータに色々なアプリケーションをインストールしたり削除したりしていると、それらが複雑に絡み合ってよくわからない状態に陥ることがあります。そうすると、自分のコンピュータでは動くけれど、他のコンピュータでは動かない、ということが起こります。Dockerを使って専用領域上でアプリケーションを動かすことでその影響を局所化することができます。

DockerでPostgreSQLを起動します。

```sh
docker run --restart=always -p 5432:5432 -d --name postgres postgres:alpine
```

これで、自分のマシン、ローカルマシン上にPostgreSQLを起動することができました。

なお、今打たなくても良いですが、PostgreSQLを止めたい時は、

```sh
docker stop postgres
```

止めた後、再開したい場合は

```sh
docker start postgres
```

と、コマンドを打ちます。

次はアプリケーションがPostgreSQLにデータを保存するようにします。

src/main/resources/config/application.yml

というファイルを作成します。
内容は以下です。

```yml
spring:
  jpa:
    database: POSTGRESQL
    show-sql: true
    hibernate:
      ddl-auto: update
  datasource:
    platform: postgres
    url: jdbc:postgresql://localhost:5432/postgres
    username: postgres
    password:
    driverClassName: org.postgresql.Driver
```

再度テストをしてみます。
MyprojectApplicationTests.javaを右クリック→Run As→JUnit Testで緑のバーが出ればOKです。

データが入っているかどうかも確認します。
コンソールで以下を実行するとデータベースの中を見ることができます。

```
docker exec postgres psql -U postgres postgres -c "select * from registation"
```

実行してみて、登録したメールアドレスが表示されれば完成です。

## 本番用データベースの作成と設定

さて、ローカル環境では動くようになりました。
本番のHeroku環境でもPostgreSQLで動くようにしてみましょう。
作業は以下です。

* Heroku環境にPostgreSQLを構築する。
* 本番用の設定ファイルを作成する。
* 本番用の設定が有効になるように環境変数を設定する。

ただし、HerokuのPostgreSQLは無料では10000のデータのみが有効です。より多くの情報を扱うときには費用が必要です。

本番データベースを用意していきます。
MyProjectのディレクトリで以下コマンドを実行します。

```
heroku addons:create heroku-postgresql:hobby-dev
```

以下のように出力されれば成功です。

```
Creating heroku-postgresql:hobby-dev on ⬢ rocky-badlands-18768... free
Database has been created and is available
 ! This database is empty. If upgrading, you can transfer
 ! data from another database with pg:copy
Created postgresql-infinite-XXXXX as DATABASE_URL
Use heroku addons:docs heroku-postgresql to view documentation
```

次に接続方法を調べるために以下のコマンドを打ちます。
postgresql-infinite-XXXXX は、上記出力のDATABASE_URLの前の文字を入力してください。

```
$ heroku pg:credentials:url postgresql-infinite-XXXXX
```
すると、以下のような出力が得られます。

```
Connection information for default credential.
Connection info string:
   "dbname=DBNAME_XXXXX host=HOST_XXXXX port=5432 user=USER_XXXXX password=PASSWORD_XXXXXX sslmode=require"
Connection URL:
   postgres://USER_XXXXX:PASSWORD_XXXXXX@HOST_XXXXX:5432/DBNAME_XXXXX
```

この出力の中の内容を本番環境の接続情報として使います。
 * dbname=に続く内容 データベース名
 * host=に続く内容 PostgreSQLがインストールされているサーバ名
 * user=に続く内容 接続ユーザ名
 * password=に続く内容 接続パスワード

本番環境の接続情報を設定知るために
src/main/resources/config/application-production.yml
というファイルを作成します。
日本語で書いた場所は上記内容に読み替えてください。

```yml
spring:
  datasource:
    url: jdbc:postgresql://サーバ名:5432/データベース名?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory
    username: 接続ユーザ名
    password: 接続パスワード
```

そして、Herokuのアプリケーションに環境変数を追加します。
アプリケーションのディレクトリで以下のコマンドを実行します。

```
heroku config:set SPRING_PROFILES_ACTIVE=production
```

少し説明をします。
これまでに2つのファイルにデータベースへの接続情報を書きました。
一つは、
src/main/resources/config/application.yml
もう一つは
src/main/resources/config/application-production.yml
です。
2つともSpring Bootの設定ファイルなのですが、application-XXXXX.ymlは、もし、環境変数のSPRING_PROFILES_ACTIVEにXXXXXが設定されていた場合、application.ymlの設定を上書きします。なので、アプリケーションの基本的な設定はapplication.ymlに書いておき、本番環境で変更になる設定値、例えば今回のようなDB接続情報をapplication-XXXXX.ymlに書くことで環境別に違う設定で動作できるという仕組みを持っています。
今回のアプリはそれを使っているのです。

これで本番環境設定は完成です。

```
git add --all #本当はサボらずに、必要なファイルを人づずつaddします。
git commit -m "DBの設定"
git push origin feature/registdb
```

GitHubでプルリクを作成、マージすると本番環境にリリースされます。

では、本番環境の動作確認をしてみましょう。

アプリケーションのディレクトリで以下コマンドを実行してください。

```
heroku open
```

ブラウザが起動し、最新版のアプリケーションが表示されていれば、うまく行っています。
メールを登録してみましょう。
メール登録後に以下コマンドを打ちます。
日本語部分は本番環境のデータベースの接続情報に置き換えてください。

```
docker exec -it postgres psql -U ユーザ名 -h サーバ名 -d データベース名 -c "select * from registation"
```
パスワードを効かれるのでデータベースのパスワードを入力します。

登録したメールアドレスが表示されたら成功です。

# 確認
* 上記の要領でメールアドレスが表示されること。
* GitHub上にコミットが記録されていること。
