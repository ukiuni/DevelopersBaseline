## リリース

いよいよ、作成したイメージを[Amazon Web Services](https://aws.amazon.com)上で動かし、世界中からアクセスできるようにします。
Amazon Web Servicesとは、クラウドサービスと呼ばれるサービスのひとつです。
クラウドサービスとは、コンピューティングリソースをインターネット上で取得できるサービスのことです。コンピュータリソースとは、たとえばコンピュータであったり、たとえばデータベースソフトであったり、機械学習のソフトウェアのことです。今日ではさまざまなコンピューティングリソースが手軽に取得できます。「クラウドファースト」という言葉があるくらい、アプリケーションを公開するときはまずクラウドを使うことが有力な公開方法です。
ほかのクラウドサービスには[Microsoft Azure](https://azure.microsoft.com)や、[Google Cloud Platform](https://cloud.google.com/)などがあります。
今回はAmazon Web Services（以下、AWS）を使います。
AWSには、さまざまなコンピューティングリソースが用意されていますが、今回使用するのはDNSサービスであるRoute53、ロードバランサの機能であるELB、そしてDockerを動かす環境であるECSです。今後別のサービスも使っていきます。
まず、ECSに作ったDockerイメージを登録します。最初にAWSにDockerイメージ登録用のリポジトリを作成します。[こちら](https://ap-northeast-1.console.aws.amazon.com/ecs/home?region=ap-northeast-1#/repositories)にアクセスして、「今すぐ始める」ボタンを押します。
![](images/aws_contener_registory_1.png)
「リポジトリ名」にmyprojectと入力して、「次のステップ」ボタンを押します。
![](images/aws_contener_registory_2.png)
「リポジトリは正常に作成されました」と表示され、その下にコマンドが表示されますので、そのコマンドをターミナルに打ち込みます。私の環境では以下です。
このコマンドは私のコマンドなので、ブラウザに表示された同等のコマンドをターミナルに打ち込むようにしてください。

```
aws ecr get-login --no-include-email --region ap-northeast-1
# 上記コマンドの結果、コマンドが表示されるので、それをターミナル上でコピー&ペーストして打ち込みます。
docker build -t myproject .
docker tag myproject:latest 831296567805.dkr.ecr.ap-northeast-1.amazonaws.com/myproject:latest
docker push 831296567805.dkr.ecr.ap-northeast-1.amazonaws.com/myproject:latest
```

次にDockerを動作させる環境をAWSに作成します。
[こちら](https://ap-northeast-1.console.aws.amazon.com/ecs/home?region=ap-northeast-1#/firstRun) にアクセスして、Next Stepを押します。
最初にリポジトリを作るように促されます。任意の名称を入力して、Next Stepを押します。
コマンドが出てくるので、そのコマンドを打っていきます。

```
aws ecr get-login --region ap-northeast-1
```

を打つと、AWSのdocker imageへ登録するためのログインコマンドが表示されるので、その通りに入力します。

```
eval $(aws ecr get-login --region ap-northeast-1)
```

と打ってもいいでしょう。次のコマンドはDockerイメージを作成するものです。すでに作成が完了しているので、飛ばしてもいいです。先程は「myProject:1.0」というタグ名でイメージを作成しました。次のコマンド、

```
docker tag myproject:latest 831296567805.dkr.ecr.ap-northeast-1.amazonaws.com/myproject:latest
```

では、イメージに新たなタグ名をつけています。Dockerでは登録するリポジトリとタグ名を関連付けることで、どこにそのイメージを登録するかを指定することができます。
最後の

```
docker push 831296567805.dkr.ecr.ap-northeast-1.amazonaws.com/myproject:latest
```

で、dockerイメージをAWSのDockerリポジトリに登録しています。
イメージの登録が終わったら、AWSコンソールに戻ってNext Stepボタンを押します。
次にTaskを作るように促されます。Task definition nameには任意の名前を、myProjectにも任意の名前を入力してください。Imageには先ほど作成したリポジトリのURLが入力されていますので、そのままに。Memory Limits (MB)もそのまま。Port mappingsには
Host portに8080、Container portに8080を、ProtocolにはTCPを入力してください。
Next stepを押して、Service nameに任意の名前を入れます。Elastic load balancingで、Container name: host portの入力欄を開くと、全ページで作成したコンテナ名と8080がついた選択肢（デフォルトだとsample-app:8080）があるのでそれを選択します。、その他はそのままでNext stepを押します。
次の画面もそのままの値で、Review & launchボタンを押し、次の画面で、Launch instance & run serviceボタンを押します。
//TODO AWSが壊れてるので、回避方法を実装。→ELBのヘルスチェックが80になっているので修正
//TODO DNSでアクセスできるように。
