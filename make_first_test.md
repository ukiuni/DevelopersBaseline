## テスト

テストでは、Firefoxブラウザを自動操作してアプリケーションが意図したとおりに動くことを確認します。ブラウザを自動操作することでアプリケーションを使う人が体験するのと同じ操作をテストします。
ブラウザでhttp://localhost:8080 にアクセスすると「新しいサービスを開始します。」と表示されることを確認します。
私事ですが、私はソフトウェアがブラウザを自動操作する様子がとても好きです。自分が楽をしている間にコンピュータが一生懸命働いてくれる感がとても好きです。
テストには[Selenium](http://www.seleniumhq.org/)というソフトウェアを利用します。

GradleにSeleniumを使うことを設定します。
build.gradleのdependenciesの中に

```groovy:build.gradle(抜粋)
testCompile 'org.seleniumhq.selenium:selenium-java:2.41.0'
```

を追加します。
追加したらターミナルでプロジェクトのディレクトリに行き、

```
gradle creanEclipse && gradle eclipse
```
と打ちます。コマンドが実行し終わったら、Eclipseのプロジェクトを選択し、右クリック→Refleshをしてください。そうするとSeleniumが使えるようになります。

次にchromedriverというchromeを動作させるためのソフトウェアをダウンロードしてきます。
https://sites.google.com/a/chromium.org/chromedriver/downloads から使っているプラットフォームのchromedriverをダウンロードしてください。
ダウンロードしたものを解凍すると、chromedriver(Windowsであればchromedriver.exe)というファイルができるので、プロジェクトのトップにlibsというディレクトリを作って置きます。Eclipseのプロジェクトを右クリック、New→Folderでlibsというディレクトリを作成し、chromedriverをドラッグ&ドロップでlibsフォルダに入れてください。
次いでchromedriverに実行権限を付与しておいてください。ターミナルにて、chromedriverを配置したディレクトリで、
```
chmod +x chromedriver
```
と入力します。
これで、Seleniumを動かす準備ができたので、次はテストコードを書いていきます。

src/test/java/com.ukiuni.education.myproject に MyprojectApplicationTestsというクラスがあります。そのクラスにメソッドを追加します。既存のメソッドは消しても構いません。
テストコードは以下です。importから始まる場所は他の部分のコードを書いておけばEclipseでCommand+Shift+oで自動で入力してくれるので、他の部分のコードを先に書くと良いです。

```java
package com.ukiuni.education.myproject;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MyprojectApplicationTests {
	private WebDriver driver;

	@LocalServerPort
	int port;

	@Before
	public void init() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "libs/chromedriver");
		driver = new ChromeDriver();
	}

	@After
	public void tearDown() {
		driver.quit();
	}

	@Test
	public void testIndex() {
		driver.get("http://localhost:" + port);
		Assert.assertEquals("新しいサービスを開始します。", driver.findElement(By.tagName("body")).getText());
	}
}
```

できたら、このクラスを右クリック、Run As→JUnit Testで実行します。
ブラウザが立ち上がりテストが実行され、Eclipseの右下のJUnitビューで緑のバーが出ていれば成功です。
![](images/junit_success.png)

赤いバーだと失敗。何かがうまく行っていません。本テキストを読みなおしてもう一回やってみましょう。
さて、これでコードが完成したので、コミットを行います。
変更があったファイルやディレクトリをgitの監視下に入れます。

```
git add build.gradle
git add src/main/java/
git add src/test/java/
git add libs
```

コミットとプッシュを行います。

```
git commit -m “予告ページの作成"
git push origin feature/noticePage
```

あとは、GitHub上でPull Requestを作成し、レビューを受け、masterにマージを行います。
これでアプリケーションが完成しました。

# 確認
上記の様にJUnitに緑のバーが出ていればOKです。
また、git logとGitHub上のCommitsに"予告ページの作成"が出ていればOKです。
