# 空のリポジトリをプッシュする。
さて、早速、作っていきます。
最初に行うのは、自分のパソコン上に作業ディレクトリを作成しGitHubにアップロードすることです。
ターミナルアプリ(Mac,Linuxは標準アプリ、Windowsの場合は[Windows System for Linux](http://www.buildinsider.net/enterprise/wsl/01)か、[Git BASH](https://git-for-windows.github.io/)を使ってください。Windowsのコマンドプロンプトは使わないでください。)を開いて、ディレクトリを作成します。
ターミナルアプリを操作するのは、コマンドによりプロジェクトを作れるようにしたいからです。コマンドで操作できることで、今後の自動化等が簡単になります。GUI操作が必要になってしまうと、自動操作が難しくなるので避けると良いでしょう。
さて、ディレクトリを作成します。

```
mkdir MyProject
cd MyProject
```

ディレクトリを作成したら、そのディレクトリをGitプロジェクト化します。

```
git init
```

gitプロジェクト化を行ったら、まず最初のコミットを行います。最初のコミットは空で行います。

```
git commit --allow-empty -m "first commit"
```

これで、空のコミットを行うことができました。

ローカルのブランチをGitHubにアップロードします。
最初にGitHubでリポジトリを作成します。ブラウザで https://github.com にアクセスしてください。まだGitHubアカウントをお持ちでない場合はSign Upからアカウントを作成してください。持っている方はSign Inからサインインしてください。その後、ヘッダの＋ボタンからNew repositoryを選択してください。
![](/images/new_repository.png)
続いて表示される画面で作りたいリポジトリ名を入力してください。名前は任意ですが、以下ではMyProjectと入力したものとして説明を進めていきます。
![](/images/create_repository.png)

もし、Organizationにリポジトリを作りたい場合はそのOrganizationを選択してください。
OrganizationとはGitHubのグループのようなものです。ユーザは複数のOrganizationに属することができ、Organizationは複数のリポジトリを持つことができます。
Privateだと限られた人のみが見ることができますが、有料です。
他の人に公開されても問題なければPublicを選択します。
入力したら、Create repositoryボタンを押します。

次に、このリポジトリにローカルブランチをアップロードします。
まず、GitHubのリポジトリをこのGitプロジェクトに設定します。

```bash
git remote add origin https://github.com/XXXXXX/MyProject.git
```

XXXXXはユーザ名です。
httpから始まるURLはGitHubで作成したリポジトリのURLです。GitHubのリポジトリのの画面で取得できます。
![](/images/repository_url.png)
このURLを、"origin"というリポジトリの名前で設定しています。
続いてGitHubにアップロードを行います。

```bash
git push origin master
```

pushというのが、別のリポジトリにGitプロジェクトを送るコマンドです。
Gitでは「ブランチ」というものを作成してソフトウェアを成長させていきます。ブランチとは日本語では枝のことです。枝を作成し、それを幹に戻していくことで開発を進めていきます。
masterというのはGitプロジェクトを作成した時に最初に作られるブランチです。git push origin masterとは、「masterブランチをorigin（上で設定したGitHubのリポジトリ）にpushします。」という意味です。

これでGitHubに空のブランチをpushすることができました。
続けて、featureブランチからmasterブランチへのPull Requestを作成し、レビューを行い、よければmasterブランチにコミットする方式を取ります。

# 確認

```
git log
```
とコマンドを打って、
```
commit 3e927d13fb59eb75a91f1c1995c96d62e5a52301 (HEAD -> master, feature/initProject)
Author: ukiuni <ukiuni@gmail.com>
Date:   Sat Nov 25 09:13:55 2017 +0900

    first commit
```
のように表示されていたら成功です。
また、GitHubのリポジトリを見て、Commitsに上記が入っていればOKです。