# TODO登録用APIの作成
いつも通りfeatureブランチを作成します。

```sh
git checkout master
git pull origin master
git checkout -b feature/registTodoApi
```

作成していきます。
先ずは、Todoを保持するためのクラスを作成します。
Eclipseのcom.ukiuni.education.myproject.entityパッケージを右クリックNew → Classを選択、NameにTodoと入力して、Finishボタンを押します。
Todoに持たせたいデータを入力していきます。内容は以下です。
例の通り、importで始まる行はCtrl + Command + o でEclipseが生成してくれるので、それ以外の場所から記載しましょう。

```java
package com.ukiuni.education.myproject.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Todo {
	@Id
	@GeneratedValue
	private long id;

	private String title;
	private String description;
	private String status;
}
```

次に、データベースにTodoを保存するためのクラスを作成します。
com.ukiuni.education.myproject.controller.repositoryを右クリック、New → Classを選択、NameにTodoRepositoryと入力して、Finishボタンを押します。

内容は以下です。

```
package com.ukiuni.education.myproject.controller.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukiuni.education.myproject.entity.Todo;

public interface TodoRepository extends JpaRepository<Todo, Long> {
}
```

最後にAPIを作成します。
com.ukiuni.education.myproject.controllerを右クリック、New → Classを選択、NameにTodoControllerと入力して、Finishボタンを押します。

TodoControllerに、Todoの登録、取得をできる機能を作成していきます。
内容は以下です。

```
package com.ukiuni.education.myproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ukiuni.education.myproject.controller.repository.TodoRepository;
import com.ukiuni.education.myproject.entity.Todo;

@RestController
@RequestMapping("api/todos")
public class TodoController {
	@Autowired
	TodoRepository todoRepository;

	@PostMapping
	public Todo create(@RequestBody Todo todo) {
		return todoRepository.save(todo);
	}

	@GetMapping
	public Iterable<Todo> loadAll() {
		return todoRepository.findAll(new Sort("id"));
	}
}
```

これでAPIが作成できました。

動作確認をしていきます。
アプリケーションが起動されていない場合MyprojectApplication.javaを右クリック、Run As→Java Applicationを押してアプリケーションを起動します。

curlコマンドでAPIを実行します。

```
curl -H 'Content-Type:application/json' -X POST -d '{"title":"タイトル","description":"説明"}' localhost:8080/api/todos
```

以下のような表示が出たら、Todoが作成されています。

```
{"id":1,"title":"タイトル","description":"説明","status":null}
```

何回か、上記コマンドを実行しておいて下さい。ターミナルで↑ボタンを押すと、前回打ったコマンドを再表示する事ができます。

それでは、全件取得してみましょう。

```
curl localhost:8080/api/todos
```

Todoがすべて出力されれば成功です。

コミットを行います。

```
git add --all #本当はサボらずに、必要なファイルを人づずつaddします。
git commit -m "Todo登録、取得APIの作成"
git push origin feature/registTodoApi
```

Pull Requestを上げてmasterにマージしてください。

これで完成です。

# 確認
上記の通り、curlコマンドで出力が得られればOKです。
また、GitHubのmasterにコミットが入っていることを確認してください。
