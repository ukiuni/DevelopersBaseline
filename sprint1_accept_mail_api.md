# サーバAPIの作成

作業の前にgit ブランチの最新化と、featureブランチの作成を行います。

```sh
git checkout master
git pull origin master
git checkout -b feature/registapi
```

開発に入ります。サーバがメールアドレスを受け取れるようにします。
3つのファイルを作成します。
* データベースに保存する型を指定するクラス、Registation
* データベースに保存する機能をもつクラス、RegistationRepository
* ブラウザからの通信を受取るクラス、RegistationController

最初に、データベースに保存するためのデータの型を作成します。

Eclipse で、src/main/javaを選択、右クリック→New→Classを選択します。
Packageに、com.ukiuni.education.myproject.entity
NameにRegistationを指定し、Finishボタンを押してください。

Nameはなんでもいいのですが、Packageは今あるMyprojectApplicationのパッケージと前方一致するパッケージ名にしてください。

内容は以下のように記載します。

importで始まる行はEclipseで Command + Shift + o を押すと自動的に補完されるので、それ以外の場所から書くと良いでしょう。

```java:Registation.java
package com.ukiuni.education.myproject.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.validator.constraints.Email;

import lombok.Data;

@Data
@Entity
public class Registation {
	@Id
	@GeneratedValue
	private long id;

	@Email
	private String mail;

	@Temporal(TemporalType.TIMESTAMP)
    public Date createdAt;

    @PrePersist
    private void onCreate() {
        this.createdAt = new Date();
    }
}
```

続いて、データベースに保存する機能をもつクラスを作成します。
Packageはcom.ukiuni.education.myproject.repositoryとしてください。
NameはRegistationRepositoryとしてください。

```java:RegistationRepository.java
package com.ukiuni.education.myproject.controller.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukiuni.education.myproject.entity.Registation;

public interface RegistaionRepository extends JpaRepository<Registation, Long> {
}
```

RegistationとRegistationRepositoryはSpring Data JPAという仕組みを利用しています。

最後にブラウザからの通信を受け取るクラスです。
Packageは、com.ukiuni.education.myproject.controller
Nameは、RegistationControllerとしてください。

```java:RegistationController.java
package com.ukiuni.education.myproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ukiuni.education.myproject.controller.repository.RegistaionRepository;
import com.ukiuni.education.myproject.entity.Registation;

@RestController
@RequestMapping("api/registrations")
public class RegistationController {
	@Autowired
	private RegistaionRepository registationRepository;

	@PostMapping
	public Registation regist(@RequestParam String mail) {
		Registation registation = new Registation();
		registation.setMail(mail);
		return registationRepository.save(registation);
	}
}
```

もし、registation.setMail(mail);の行に赤い下線が出ていたら、[Lombok](https://projectlombok.org/)の設定ができていません。lombokのjarをダウンロード、ダブルクリックして、Eclipseを指定、インストールを行い、Eclipseを再起動してください。

これでサーバは完成です。MyprojectApplicationを右クリック→Run As→Java Applicationを押してサーバを起動してください。

それではAPIを実行してみます。
ターミナルで以下を実行してください。

```
curl -X POST -d "mail=test@example.com" localhost:8080/api/registrations
```

以下のような表示が出力され、

```
{"id":1,"mail":"test@example.com"}
```

idが付与され、JSONが返却されているのが確認できれば完成です。

コミットを行います。

```
git add --all #本当はサボらずに、必要なファイルを人づずつaddします。
git commit -m "事前メール登録機能の作成"
git push origin feature/registapi
```

push できたら、GitHubでPull Requestを作成して、レビューを受け、マージをしてもらってください。（一人で作る時は、一人二役で。）

Pull Requestがmasterにマージされると、herokuに自動的にリリースされているはずです。
アプリケーションのディレクトリで以下コマンドを実行してください。

# 確認
* 上記の要領でJSONが返却されること。
* GitHubでコミットを確認できること。

上記が確認できればOKです。
