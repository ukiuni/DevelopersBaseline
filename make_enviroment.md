## 環境構築
以下をインストールしてください。
コマンドはターミナルやコマンドプロンプトで実行可能な事を確認してください。
具体的なやり方はインターネットを調べれば多量に出てきます。自分で調べてやってください。
また、最初にすべてインストールしなくても使う時にインストールしても大丈夫です。

* [Java Development Kit (JDK 1.8)](https://www.azul.com/downloads/zulu/)
 * 確認 コマンド javac -version を実行してバージョンが表示されること。
* [Gradle](https://gradle.org/)
 * 確認 コマンド gradle -version で、バージョンが表示されること。
* [Eclipse](https://www.eclipse.org)
 * 確認 Eclipseが起動されること。
* [Git コマンド](https://git-scm.com/)
 * 確認 コマンド git --version で、バージョンが表示されること。
* [GitHub](https://github.com/) アカウント
 * 確認 サイトにログインできること。
* [Lombok](https://projectlombok.org/)
 * 確認 lombok-xxx.jarをダブルクリック、インストールされているEclipseを指定してInstall/Updateが押されていること。
* [Chrome](https://www.google.co.jp/chrome/browser/desktop/index.html)
 * 確認 Chromeが起動できること
* [Heroku](https://www.heroku.com/) アカウント
 * 確認 サイトにログインできること。
* [Heroku cli](https://devcenter.heroku.com/articles/heroku-cli)
 * 確認 コマンド heroku --version で、バージョンが表示されること。
* [Travis CI](https://travis-ci.org/) アカウント
 * 確認 サイトにログインできること。
* [The Travis Client](https://github.com/travis-ci/travis.rb#installation)
 * 確認 コマンド travis -version で、バージョンが表示されること。
* [Docker](https://www.docker.com/)
 * 確認 コマンド docker run hello-world で、helloを含む長々とした表示がされること。

# 困ったら
コマンドを打っても、期待した結果が得られないことがあるでしょう。
その際は、そのコマンドのインストールに必要な作業をもう一度最初からやり直してみましょう。
ダウンロードが必要なモノだとしたら、そのダウンロードするファイルをインターネットからダウンロードするところからやり直してみましょう。
一度壊れたものを治すより、イチからやり直すほうが簡単なことが往々にあるからです。
以降のページでも、うまくいかないな、と思ったら、イチからやり直してみると良いでしょう。