# 画面の作成

次に画面を作成します。
作業の前にgit ブランチの最新化と、featureブランチの作成を行います。

```sh
git checkout master
git pull origin master
git checkout -b feature/registmail
```

ブラウザ上で画面を作成するライブラリ、フレームワークは様々ありますが、今回[Vue.js](https://jp.vuejs.org/index.html)と[axios](https://github.com/axios/axios)というフレームワークを使用します。Vue.jsは画面描画用のライブラリで、
この手のライブラリ、フレームワークにはほかに[AngularJS](https://angularjs.org/)、[React](https://reactjs.org/)など、凄まじくたくさんのフレームワークがあります。調べてみても良いかもしれません。
axiosは通信用のライブラリです。
アプリからVue.jsとAxiosを利用可能なようにbuild.gradleに依存性を追加します。
Vue.js、axiosを利用できるようにbuild.gradleのdependenciesの中に、

```groovy
runtime group: 'org.webjars', name: 'vue', version: '2.5.3'
runtime group: 'org.webjars.npm', name: 'axios', version: '0.17.0'
runtime group: 'org.webjars.npm', name: 'vuedraggable', version: '2.14.1'
runtime group: 'com.ukiuni', name: 'spring-boot-view-injector', version: '0.0.7'

```
を追加します。
最後のspring-boot-view-injectorは筆者作成のライブラリで、Vue.jsを簡単に使えるようにしてくれています。標準的ではありませんが、現状の代替策があまりにも煩雑なので、本書ではspring-boot-view-injectorを使用します。

追加したら、コマンドを実行し、eclipseにライブラリを取り込みます。

```sh
gradle cleanEclipse && gradle eclipse
```

コマンドを実行したら、Eclipseのプロジェクトを選択してF5を押してください。
もし、アプリを実行中であれば、一度停止しておいてください。

これでJavaScript系のライブラリが使えるようになりました。
早速、index.htmlをカスタマイズして、メールアドレスを登録できるようにします。

```html:index.html
<!DOCTYPE html>
<html>

<head>
	<meta charset=utf-8>
	<title>新しいサービス</title>
	<script type="text/javascript" src="webjars/vue/2.5.3/vue.min.js"></script>
	<script type="text/javascript" src="webjars/axios/0.17.0/dist/axios.min.js"></script>
</head>

<body>
	<div id="contents">
	</div>
</body>
<script type="text/javascript" src="js/app.js"></script>

</html>

```
メールを登録する画面はindex.htmlとは切り離して作成しておきます。
src/main/resources/static/templates/registMail.html というファイルを作成してください。

```java:registMail.html
<div id="read">新しいサービスを開始します。</div>
<div v-show="!registedMail">
	<div v-show="error">{{error}}</div>
	<input type="email" v-model="mail" placeholder="招待用メール" />
	<button @click="regist(mail)">登録</button>
</div>
<div id="thx"v-show="registedMail">お知せは {{registedMail}} にメーをお送りします。連絡差し上げるでしばらくお待ち下さい。
</div>
```

また、src/main/resources/static/js/registMail.js というファイルを作成し、以下を記載します。

```javascript:registMail.js
function registMail() {
    new Vue({
        el: '#contents',
        /** @inject("templates/registMail.html") */
        template: null;
        data: {
            mail: "",
            error: null,
            registedMail: null
        },
        methods: {
            regist: function (mail) {
                error = null;
                axios.post("api/registrations", "mail=" + mail).then(
                    () => {
                        this.registedMail = mail;
                    }).catch((e) => {
                        this.error = "登録できないメールアドレスです。";
                    });
            }
        }
    });
}
```

さらに、src/main/resources/static/js/app.js というファイルを作成します。

```
/** @injectJS("js/registMail.js") */
const app = null;
```

これで、app.jsからregistMail.jsを読み込んでいます。
/** @inject("templates/registMail.html") */
や、
/** @injectJS("js/registMail.js") */
は、それに続く変数に他のHTMLやJavaScriptを代入してくれます。
JavaScriptはブラウザ上で動くものです。JavaScript自体でも、他のJavaScriptを読み込むことはできますが、サーバにアクセスするのに時間がかかったり、非同期にしかアクセスできなかったりします。
@injectを使うことで、ブラウザに読み込まれる前にJavaScriptを結合してくれるので便利です。
また、@injectはJavaScriptの結合以外にも、HTMLに、CSSや画像を結合してくれるので、通信量の削減や速度向上が期待できます。


さて、これで完成です。
ブラウザを開いて、画面でメールアドレスを入力し、登録ボタンを押すと、サーバにメールアドレスが送信されるようになりました。

ブラウザで http://localhost:8080
にアクセスします。

![](images/regist_start.png)
メールを入れてみます。
![](images/regist_registed.png)
これで完成です。

## テストの作成

続いて、テストを作りましょう。
MyprojectApplicationTests.javaに追記を行います。

```java:MyprojectApplicationTests.java
package com.ukiuni.education.myproject;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MyprojectApplicationTests {
	private WebDriver driver;

	@LocalServerPort
	int port;

	@Before
	public void init() throws InterruptedException {
		String os = System.getProperty("os.name").toLowerCase();
		if (os.contains("linux")) {
			System.setProperty("webdriver.chrome.driver", "libs/chromedriver_linux");
		} else if (os.contains("mac")) {
			System.setProperty("webdriver.chrome.driver", "libs/chromedriver");
		}
		driver = new ChromeDriver();
	}

	@After
	public void tearDown() {
		driver.quit();
	}

	@Test
	public void testIndex() {
		driver.get("http://localhost:" + port);
		Assert.assertEquals("新しいサービスを開始します。", driver.findElement(By.id("read")).getText());
	}

	@Test
	public void testRegist() {
		driver.get("http://localhost:" + port);
		driver.findElement(By.tagName("input")).sendKeys("asdf@example.com");
		driver.findElement(By.tagName("button")).click();
		new WebDriverWait(driver, 10).until(ExpectedConditions.textToBe(By.id("thx"), "お知らせは asdf@example.com にメールをお送りします。連絡差し上げるまでしばらくお待ち下さい。"));
	}

	@Test
	public void testRegistError() {
		driver.get("http://localhost:" + port);
		driver.findElement(By.tagName("input")).sendKeys("asd");
		driver.findElement(By.tagName("button")).click();
		new WebDriverWait(driver, 10).until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"), "登録できないメールアドレスです。"));
	}
}
```
メールが登録できていることを確認するテストと共に、メールアドレスでないものを入力された時にエラーが表示されることも確認しています。

MyprojectApplicationTests.javaを右クリック→Run As→JUnit Testで緑のバーが出れば完成です。

これでソフトウェアの作成は完了です。

コミットを行います。

```
git add --all #本当はサボらずに、必要なファイルを人づずつaddします。
git commit -m "事前メール登録機能の作成"
git push origin feature/registmail
```

push できたら、GitHubでPull Requestを作成して、レビューを受け、マージをしてもらってください。（一人で作る時は、一人二役で。）

Pull Requestがmasterにマージされると、herokuに自動的にリリースされているはずです。
アプリケーションのディレクトリで以下コマンドを実行してください。

```
heroku open
```

ブラウザが起動し、最新版のアプリケーションが表示されていれば、うまく行っています。
メールを登録してみましょう。
メール登録後に以下コマンドを打ちます。
日本語部分は本番環境のデータベースの接続情報に置き換えてください。

```
docker exec -it postgres psql -U ユーザ名 -h サーバ名 -d データベース名 -c "select * from registation"
```
パスワードを効かれるのでデータベースのパスワードを入力します。

登録したメールアドレスが表示されたら成功です。

# 確認
* テストが通っていること。
* GitHubのmasterブランチにコミットが入っていること。
* 上記の様に本番環境のデータベースにメールアドレスが登録されていることが確認できていること。
を確認してください。
