# プロジェクトの初期化
[Gradle](https://gradle.org/)と[Spring Boot](http://projects.spring.io/spring-boot/)という仕組みを使ってアプリケーションを作っていきます。

先ずは、GitHubのリポジトリと自分のリポジトリの同期を取ります。

```
git checkout master
git pull origin master
```

次に、GradleとSpring Bootを使えるようにプロジェクトに機能追加するので、Gitのfeatureブランチを作成します。機能開発ブランチです。

```bash
git branch feature/initProject
git checkout feature/initProject
```

これで、masterブランチを元にfeature/initProjectブランチが作成されました。このブランチを更新、コミットを行い、最終的にmasterと比較してレビューを行い、masterに取り込むことで開発を進めていきます。

次に、GradleとSpring Bootを使えるようにします。 [SPRING INITIALIZER](https://start.spring.io) にブラウザでアクセスしてください。
![](images/spring_initializer.png)
このサイトでは、Spring Bootを使うためのテンプレートアプリケーションをダウンロードする事ができます。
Generate a "Gradle Project" with "Java" and Spring Boot "1.5.9"
を指定してください。
Groupに、com.ukiuni.education　(なんでも構いませんが、ユニークにすると良いです。パッケージ名の先頭になります。)
Artifactに myproject と入力します。
Search for Dependenciesに、
Web, JPA, H2, PostgreSQL, DevTools, Lombok
を指定(一つづつ、打った後にエンターキーを打ちます。「Web」→エンターキー、「JPA」→エンターキー、のように。)し、Generate Projectをクリックします。
するとmyproject.zipというファイルがダウンロードできます。
myproject.zipをダウンロードしたら、解凍し、中身を前章で作成したディレクトリにコピーします。マウスでドラッグアンドドロップしてもいいですし、下記コマンドをプロジェクトのディレクトリで実行しても良いです。

```
unzip -j -d . ${myproject.zipへの絶対パス}
```

これでSpring BootとGradleが使えるようになりました。

Gradleとはビルドツールと呼ばれているものの一つです。
ソフトウェアは、ソースコードから適切に動くように組み立てる必要があります。この作業をビルドと言います。ビルドツールとはビルドを手伝ってくれるツールです。
Javaのビルドツールの有名なものにはAnt、Maven、Gradleがあります。
Antはビルドの手順をXMLに書いておく必要があります。
Mavenはビルド手順やディレクトリ構成に決まりを持たせることで、ある程度手順を記載しなくてもビルドができます。ただし、ライブラリを使用したい場合はやはりXMLを記載する必要があります。
そして、現在JavaのビルドツールといえばGradleです。XMLを記載しなくても良くなりました。その代わりにGroovyというプログラミング言語でビルド手順を記載します。
他のツールにおいてもそうですが、現在においてXMLを記載しなくては使えないツールは往々にして時代遅れと思えます。XMLを記載しなければならないか、そうでないかを、技術を評価する際の指針にすると良いでしょう。

さて、プロジェクトの中身を見てみましょう。

```
.
├── build.gradle
├── gradle
│   └── wrapper
│       ├── gradle-wrapper.jar
│       └── gradle-wrapper.properties
├── gradlew
├── gradlew.bat
└── src
    ├── main
    │   ├── java
    │   │   └── com
    │   │       └── ukiuni
    │   │           └── education
    │   │               └── myproject
    │   │                   └── MyprojectApplication.java
    │   └── resources
    │       ├── application.properties
    │       ├── static
    │       └── templates
    └── test
        └── java
            └── com
                └── ukiuni
                    └── education
                        └── myproject
                            └── MyprojectApplicationTests.java
```

この中で一番重要なファイルは build.gradleです。
Gradleではbuild.gradleに必要なライブラリを記載することで、ビルド時に必要なライブラリをダウンロード、取り込みをしてくれます。
現在のbuild.gradleには SPRING INITIALIZER が必要なライブラリを入れてくれているので、そのまま使用します。
その他で重要なのが、src/main/java、src/main/resources、src/test/javaでしょうか。gradleではフォルダ構成が決まっており、javaのソースはsrc/main/javaに入れておけばコマンドがコンパイルを行ってくれます。
また、src/test/javaにはjavaのテストコードを配置します。たとえばJUnitのテストコードを配置しておけば、コマンドがテストを探し出して実行してくれます。
このようにファイルの配置する場所をルール化することで、どのプロジェクトでも画一的にソースコードを読めるようにしているのです。

## コミットとプッシュを行う。
プロジェクトでGradleが使えるようになったので、コミットとプッシュを行います。

```bash
git add --all
git commit -m "Gradleプロジェクトの設定"
```
コマンドを解説していきます。
Gitではコミットの前に、コミットするファイルに印をつけます。それがgit add です。git add するとindexという場所にファイルが配置され、次のcommitでファイルがコミットされます。通常は

```
git add ファイル名
```

の様にしてファイルをindexに入れますが、今回はファイルが多いので、手抜きをして--addで全てのファイルをindex化しました。手抜きをすると間違って必要のないファイルをcommitすることがあるので、以降はしません。
続いて、

```
git commit -m "コミットメッセージ"
```

でファイルをcommit、コミットします。コミットすることでファイルの状態が保存されます。

git log を打つとコミットを確認することができます。

## GitHubにプッシュする。

引き続き、feature/initProject ブランチをGitHubにアップロードします。Gitでのアップロードはプッシュといい、ブランチ単位でプッシュは行います。

```bash
git push origin feature/initProject
```

これでGitHubに作業がプッシュされました。
この変更をGitHub上でmasterに入れます。
ブラウザでhttps://github.comにアクセスし、作成したリポジトリを開いてください。
上方のメニューのPull Requestを開いて、New pull requestボタンを押し、masterブランチとfeature/initProjectを比較、Create pull requestを押してください。
どんな変更をしたか入力する欄が表示されるため、「Gradleプロジェクトの設定」と入力し、Create pull requestボタンを押してください。するとPull Requestが作成されます。
Pull Requestとはその名の通り、とり込み要求です。Pull Requestの内容が正しいかを誰か他の人に見てもらってください。この見てもらう作業の事をレビューと言います。
レビューをする人は、正しいことを確認してください。この変更が何故正しいといえるかを考えて、もし質問等があったら、このPull Requestにコメントを入れてください。ソースコードの行をクリックすると、そこにコメントを入れることができますし、Pull Request全体に対するコメントでしたら、Pull RequestのConversationタブから入力することができます。レビューする人は責任を持って正しいといえるまでしっかり見てください。大丈夫そうなら「大丈夫そう」とコメントを入れてMerge pull requestボタンを押してください。これにより、feature/initProjectブランチがmasterブランチに取り込まれます。

これが一連のコミットの流れです。

# 確認
```
gradle bootRun
```
を実行し、ブラウザにて http://localhost:8080 にアクセスします。
![](images/spring404.png)
が表示されればOKです。確認後、Ctrl+Cをキー入力してプログラムを終了させておいてください。

また、

```
git log
```

で「Gradleプロジェクトの設定」が表示されており、GitHubのCommitsにも「Gradleプロジェクトの設定」が表示されていればOKです。