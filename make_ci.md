# CI/CDの設定
CI、Continuous Integration とは繰り返しソフトウェアを結合し動かすことで常にソフトウェアの状態を正常に保つこと、CDとは繰り返しリリースしていくことです。
ここでは、
* GitHubにPull Requestを作成すると自動的にテストされ、テスト結果がGitHubに登録される
* Pull RequestがmasterブランチにマージされるとHerokuにデプロイされる
状態を設定します。

作業の前にgit ブランチの最新化と、featureブランチの作成を行います。

```sh
git checkout master
git pull origin master
git checkout -b feature/CIWithTravis
```

CI/CDには[Travis CI](https://travis-ci.org/)を利用します。
Travis CIにGitHubアカウントでログインをしておいてください。
右上の自分のアイコンをクリックし、Accountページを表示、
MyProjectをonにしてください。
![](images/travis_on.ong)

続いて、Travis CIでのビルド方法を設定します。
Travis CIの設定はアプリケーションのディレクトリに.travis.ymlというファイルを作成し、そこで設定します。
「.」から始まるファイルを作成できない場合は、[vi](https://net-newbie.com/linux/commands/vi.html)を使ってみてください。

```yml:.travis.yml
os: linux
language: java
jdk:
- openjdk8
env: DISPLAY=':99.0'
addons:
  chrome: stable
before_script:
- sh -e /etc/init.d/xvfb start
script:
- "./gradlew test"
```

これでビルド方法が設定できました。xvfbという仮想画面アプリを使っているので設定が少し長くなっています。

次にコマンドを打ちます。

```
$ travis setup heroku
Deploy only from ukiuni/MyProject? |yes| yes
Deploy from feature/CIWithTravis branch? |yes| yes
Encrypt API key? |yes| yes
```

これで.travis.ymlにHerokuへのデプロイ設定が行われます。ただし、このままだとfeature/CIWithTravisにpushした時にデプロイされてしまうので、
branch: feature/FitTravis
を
branch: master
に変更します。

全体にはこのような感じです。

```yml:.travis.yml
os: linux
language: java
jdk:
- openjdk8
env: DISPLAY=':99.0'
addons:
  chrome: stable
before_script:
- sh -e /etc/init.d/xvfb start
script:
- "./gradlew test"
deploy:
  provider: heroku
  api_key:
    secure: 長ーーーい、鍵
  app: rocky-badlands-18768
  on:
    repo: ukiuni/MyProject
    branch: master # ここをfeature/FitTravis → masterに書き換えます。
```

これで、CI/CDの設定は完了です。

```
git add .travis.yml
git commit -m "Travisの設定"
git push origin feature/FitTravis
``` 

で、GitHubにpushして、GitHubでプルリク作成、マージを行ってください。

# 確認
Pull Requestで
![](images/github_ci.png)
と出ていれば、CIがされています。下の方です。黄色い時は処理中です。
また、heroku open すると、本番環境が最新バージョンになっていることが確認できますが、今はアプリそのものはいじってないのでわかりません。
次にアプリを更新した時に確認しましょう。