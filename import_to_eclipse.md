# プロジェクトをEclipseに読み込む
EclipseはIDEと呼ばれる開発を補助してくれるツールです。保存時の自動コンパイルやコンパイルチェック、コーディング時の自動補完やソースコードフォーマット等、様々な機能があります。
IDEには、他にも[IntelliJ IDEA](https://www.jetbrains.com/idea/) や [NetBeans](https://ja.netbeans.org/) 等があります。単なるエディタでソースコードを書くこともできますが、とても便利なのでIDEを使っていきます。
Eclipseがプロジェクトを読み込むためにはEclipse用の設定ファイルが必要です。Eclipseの設定ファイルはGradleがbuild.gradleの内容から生成してくれます。生成のコマンドは以下です。

```bash
gradle eclipse
```

これで、ディレクトリがEclipseプロジェクトになりました。ディレクトリの中に、.classpathや.projectなどのファイルが出来上がっています。
ディレクトリをEclipseで開いてみましょう。Eclipseを起動し、ツールバーのFile→Import→Existing Project into Workspace、Select root directoryで先ほど作成したディレクトリを指定し、Finishボタンを押します。これで先ほど作成したプロジェクトがEclipseで編集できるようになりました。

# 確認
EclipseにMyProjectが表示されていて、その中のsrc/main/java/com.ukiuni.education.myproject/MyProjectApplication.java を右クリック、Run As → Java Applicationにて起動します。ブラウザにて http://localhost:8080 にアクセスし、
![](images/spring404.png)
が表示されればOKです。確認後、Ctrl+Cをキー入力してプログラムを終了させておいてください。
確認後、コンソールの赤い四角ボタンを押してアプリケーションを終了して置いてください。

